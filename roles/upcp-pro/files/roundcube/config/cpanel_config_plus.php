<?php

/*
This file is included from the main config.inc.php and attempts to ensure
that mandatory values still exist even when the config.inc.php has been modified
*/

$mandatory_plugins = ['cpanellogin','cpanellogout','automatic_addressbook_ng','html5_notifier','archive','calendar','twofactor_gauthenticator','return_to_webmail','carddav','markasjunk','xframework','xadvert','xbackground','xlast_login','xnews_feed','xweather','xquote','xcompose','xskin'];

foreach( $mandatory_plugins as $plugin ) {
    if(! preg_grep("/$plugin/",$config['plugins']) ) {
        array_push($config['plugins'],$plugin);
    }
}

$mandatory_skin = ['outlook_plus','gmail_plus'];

foreach( $mandatory_skin as $skin ) {
    if(! preg_grep("/$skin/",$config['skin']) ) {
	array_push($config['skin'],$skin);
    }
}

$mandatory_skins_allowed = ['outlook_plus','gmail_plus'];

foreach( $mandatory_skins_allowed as $skins_allowed ) {
    if(! preg_grep("/$skins_allowed/",$config['skins_allowed']) ) {
	array_push($config['skins_allowed'],$skins_allowed);
    }
}

$config['license_key'] = 'RCP-zG68ygUwD7nd';

?>
