#!/bin/bash
# Set verbose to null
verbose=""

# Print the help text
helptext () {
    tput bold
    tput setaf 2
    echo "[cPanel - IPv6 Address Settings]"
    echo "USAGE: $0 [Options] [Domain]"
    echo "Options:"
    echo "  -h, --help  Display this help and exit"
    echo "  -e, --enable  Enable IPv6"
    echo "  -d, --disable Disable IPv6"
    echo "."
    echo "."
    echo "."
tput bold
tput setaf 1
echo "- Only run 1 time!"
    tput sgr0
    exit 0
}

# Check Domain
enable () {
    domain=$1
    account=`grep -E ^$domain /etc/userdomains | sed 's/://g' | awk '{print $2}'`
    # Check Domain
    if [ -z $account ]; then
        echo -e "--- Domain $1 does not exist on Server---"
        exit 1
    else
        # Enable IPv6
        whmapi1 --output=jsonpretty ipv6_enable_account user=$account range='SHARED' > /dev/null 2>&1
        echo -e "IPv6 of Domain $1 has been enabled"
    fi
}

disable () {
    domain=$1
    account=`grep -E ^$domain /etc/userdomains | sed 's/://g' | awk '{print $2}'`
    # Check Domain
    if [ -z $account ]; then
        echo -e "--- Domain $1 does not exist on Server---"
        exit 1
    else
        # Disable IPv6
        whmapi1 --output=jsonpretty ipv6_disable_account user=$account range='SHARED' > /dev/null 2>&1
        echo -e "IPv6 of Domain $1 has been disabled"
    fi
}

# Check the domain parameters are passed
if [ -z "$2" ] ;then
    tput bold
    tput setaf 1
    echo "Invalid Option!"
    helptext
    exit 1
fi

# Main function, switches options passed to it
case "$1" in
    -h) helptext
    ;;
    --help) helptext
    case "$2" in
        --enable) enable "$3"
        ;;
        -e) enable "$3"
        ;;
        --disable) disable "$3"
        ;;
        -d) disable "$3"
        ;;
        *)
            tput bold
            tput setaf 1
            echo "Invalid Option!"
            helptext
        ;;
    esac
        ;;
        --enable) enable "$2"
        ;;
        -e) enable "$2"
        ;;
        --disable) disable "$2"
        ;;
        -d) disable "$2"
        ;;
            *)
            tput bold
            tput setaf 1
            echo "Invalid Option!"
            helptext
    ;;
esac
