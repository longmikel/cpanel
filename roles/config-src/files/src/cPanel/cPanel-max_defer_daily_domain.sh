#!/bin/bash
# Use domain
if [ -z "$1" ]; then
    echo Usage: $0 {domain}
    exit 1
fi

# Check user exist
echo "Preparing to check domain $1, please wait..."
sleep 3
i=`grep -E ^$1 /etc/userdomains | sed 's/://g' | awk '{print $2}'`
if [ -z $i ]; then
    echo -e "--- Domain $1 does not exist ---"
    exit 1
else
    echo -e "--- Domain $1 exist ---"
fi

# Check and remove file email send limits
echo "Preparing to check and remove file email send limits of domain $1, please wait..."
sleep 3
if [ -f /var/cpanel/email_send_limits/daily_notify/$1 ]; then
    rm -rf /var/cpanel/email_send_limits/daily_notify/$1
    rm -rf /var/cpanel/email_send_limits/daily_notify/"$1"_send
else
    echo -e "--- File email send limits of domain $1 does not exist ---"
fi

echo -e "--- Done ---"
exit 0
