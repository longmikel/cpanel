#!/bin/bash
# Set verbose to null
verbose=""

# Print the help text
helptext () {
    tput bold
    tput setaf 2
    echo "[Mailbox] Check ratelimit"
    echo "USAGE: $0 [Options]"
    echo "Options:"
    echo "  -h, --help  display this help and exit"
    echo "  -r, --reset-default-limit  reset default rate-limit"
    echo "  -s, --set-limit   set new limit for mailbox"
    echo "."
    echo "."
    echo "."
      tput bold
      tput setaf 1
      echo "- Run time every day!"
    tput sgr0
    exit 0
}

mailbox_set_ratelimit () {
  # Check mailbox parameters are passed
    if [ -z "$1" ]; then
    tput bold
    tput setaf 2
    echo Usage: $0 [option] {mailbox}
    echo "  -s, --set-limit   set new limit for mailbox expanded +1000"
    echo "."
      tput bold
      tput setaf 1
      echo "- Run time with mailbox have reached the limit of 1000!"
    tput sgr0
    exit 1
  fi
  # Check mailbox existence in limit setting
  mailbox=$1
  mailbox_rate="$(grep -E "$mailbox.*Sender rate too many messages in 1 day" /var/log/exim_mainlog | sort -r | head -n1 | grep -Eo '(0|[1-9][0-9]*).(0|[1-9][0-9]*)\/(0|[1-9][0-9]*).(0|[1-9][0-9]*)' | cut -d'/' -f1 | cut -d'.' -f1)"

  i_mailbox=`grep -E ^$mailbox /etc/send_limits | sed 's/://g' | awk '{ print $1 }'`

  if [[ -z $i_mailbox ]]; then
    # Check mailbox have reached the limit of 1000
    if [[ $mailbox_rate < 1000 ]]; then
      echo -e "Mailbox $mailbox have not reached the limit of 1000"
      exit 1
    else
      echo -e "Mailbox $mailbox has reached the limit of $mailbox_rate/1000"
      mailbox_limit=$(expr $mailbox_rate + 1000)
      sed -i "3s/^/$mailbox: $mailbox_limit\n/" /etc/send_limits
    fi
  else
    i_limit=`grep -E ^$mailbox /etc/send_limits | sed 's/://g' | awk '{ print $2 }'`
    if [[ $i_limit < $mailbox_rate ]]; then
      echo -e "Mailbox $mailbox is being set limit $i_limit should be expanded +1000"
      sed -i "s/$i_limit/$(expr $i_limit + 1000)/" /etc/send_limits
    else
      echo -e "Mailbox $mailbox have limit setting $i_limit > rate is limited $mailbox_rate so it can't be expanded"
      exit 1
    fi
  fi
}

mailbox_default_ratelimit () {
  # Check none-parameters with reset-default-limit
  if [ -n "$1" ]; then
    tput bold
    tput setaf 2
    echo Usage: $0
    echo "  -r, --reset-default-limit  reset default rate-limit"
    echo "."
      tput bold
      tput setaf 1
      echo "- Run time every day!"
    tput sgr0
    exit 1
  fi

cat << EOF > /etc/send_limits
# Commented lines and blank lines are ignored
# Format is     EMAIL: LIMIT

# Must be the last line, this is the default limit
*: 1000
EOF
}

# Main function, switch options passed to it
case "$1" in
  -h) helptext
  ;;
  --help) helptext

  case "$2" in
    --reset-default-limit) mailbox_default_ratelimit "$3"
    ;;
    -r) mailbox_default_ratelimit "$3"
    ;;
    --set-limit) mailbox_set_ratelimit "$3"
    ;;
    -s) mailbox_set_ratelimit "$3"
    ;;
  *)
    tput bold
    tput setaf 1
    echo "Invalid Option!"
    helptext
    ;;
  esac
    ;;
    --reset-default-limit) mailbox_default_ratelimit "$2"
    ;;
    -r) mailbox_default_ratelimit "$2"
    ;;
    --set-limit) mailbox_set_ratelimit "$2"
    ;;
    -s) mailbox_set_ratelimit "$2"
    ;;
  *)
    tput bold
    tput setaf 1
    echo "Invalid Option!"
    helptext
    ;;
esac
###########   >_   #####################################################################
