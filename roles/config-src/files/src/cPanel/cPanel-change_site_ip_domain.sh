#!/bin/bash
FILE=/etc/trueuserdomains
IP=$(cat /var/cpanel/mainip)

{ while IFS=' ' read DOMAIN USER
    do
        /usr/local/cpanel/bin/setsiteip -u $USER $IP
    done
} < $FILE
