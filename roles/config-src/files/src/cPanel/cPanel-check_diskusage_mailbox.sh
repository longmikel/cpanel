#!/bin/bash
GREEN='\e[32m'
STD='\e[39m'

> /usr/local/src/user-diskusage.txt

# Use domain
if [ -z "$1" ]; then
    echo Usage: $0 {domain}
    exit 1
fi

# Check domain exist
echo "Preparing to check domain $1, please wait..."
sleep 3
i=`grep -E ^$1 /etc/userdomains | sed 's/://g' | awk '{print $2}'`
if [ -z $i ]; then
    echo -e "\n --- Domain $1 does not exist ---"
    exit 1
else
    echo -e "\n --- Domain $1 exist ---"
fi


ls -d /home/$i/mail/$1/*/ | awk 'BEGIN {
}
{
tcmd = "test -d " $1
if(!system(tcmd)){
split($1,MyArray,"/")
print MyArray[6] "@" MyArray[5]
}
}
'|while read Email

do
    Domain="$(echo "${Email}" | cut -d'@' -f2)"
    Account="$(cat /etc/userdomains | grep "${Domain}" | cut -d' ' -f2)"
    User="$(echo "${Email}" | cut -d'@' -f1)"
    diskused=`uapi --user=$Account Email get_disk_usage user=$User domain=$Domain | grep diskused | awk '{print $2}' | sed "s/^'//;s/'$//"`
    echo -e "Domain: $Domain \nUser: $Email \nDisk_used: $diskused M \n" >> /usr/local/src/cPanel/user-diskusage.txt
done;
echo -e "\n ----- Email's Disk Usage -----"
cat /usr/local/src/cPanel/user-diskusage.txt
