#!/bin/bash
if [ -z "$1" ] ; then
    echo Usage: $0 {domain.com}
    exit 1
fi

ACCOUNT=`whmapi1 --output=jsonpretty getdomainowner domain=$1 | grep user | awk '{print $3}' | sed 's/"//g'`

echo " ----- The '$1' was owned by account '$ACCOUNT' ----- "
