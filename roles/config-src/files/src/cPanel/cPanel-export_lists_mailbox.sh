#!/bin/bash
# Use domain
if [ -z "$1" ]; then
    echo Usage: $0 {domain}
    exit 1
fi

# Check domain exist
echo "Preparing to check domain $1, please wait..."
sleep 3
i=`grep -E ^$1 /etc/userdomains | sed 's/://g' | awk '{print $2}'`
if [ -z $i ]; then
    echo -e "--- Domain $1 does not exist ---\n"
    exit 1
else
    echo -e "--- Domain $1 exist ---\n"
fi

uapi --output=jsonpretty --user=$i Email list_pops no_validate=0 skip_main=1 | jq -r '.result.data[].email'
total1=$(uapi --output=jsonpretty --user=$i Email list_pops no_validate=0 skip_main=1 | jq -r '.result.data[].email' | wc -l)

echo -e "\n--- Total Mailbox: $total1 ---"
echo -e "--- Done ---"
exit 0
