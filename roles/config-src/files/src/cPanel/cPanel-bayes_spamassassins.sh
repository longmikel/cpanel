#!/bin/sh
# Use domain
if [ -z "$1" ]; then
    echo Usage: $0 {domain}
    exit 1
fi

# Check domain exist
echo "Preparing to check domain $1, please wait..."
sleep 3
i=`grep -E ^$1 /etc/userdomains | sed 's/://g' | awk '{print $2}'`
if [ -z $i ]; then
    echo -e "--- Domain $1 does not exist ---"
    exit 1
else
    echo -e "--- Domain $1 exist ---"
fi

# Force a database sync and expiry run
echo "Preparing to force a database sync and expiry run, please wait..."
sleep 3
/usr/local/cpanel/3rdparty/bin/sa-learn --force-expire
echo -e "--- Done ---"

# Check and remove 3 file bayes_journal - bayes_seen - bayes_toks
echo "Preparing to check and remove 3 file bayes_journal - bayes_seen - bayes_toks, please wait..."
sleep 3
## Check file bayes_journal
if [ -f /home/$i/.spamassassin/bayes_journal ]; then
    rm -rf /home/$i/.spamassassin/bayes_journal
else
    echo -e "--- Files bayes_journal does not exist ---"
fi

## Check file bayes_seen
if [ -f /home/$i/.spamassassin/bayes_seen ]; then
    rm -rf /home/$i/.spamassassin/bayes_seen
else
    echo -e "--- Files bayes_seen does not exist ---"
fi

## Check file bayes_toks
if [ -f /home/$i/.spamassassin/bayes_toks ]; then
    rm -rf /home/$i/.spamassassin/bayes_toks
else
    echo -e "--- Files bayes_toks does not exist ---"
fi
echo -e "--- Done ---"

# Restart Spamd
echo "Preparing to restart spamd, please wait..."
sleep 3
/scripts/restartsrv_spamd
echo -e "--- Done ---"
exit 0
