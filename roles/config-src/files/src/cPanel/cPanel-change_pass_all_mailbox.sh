#!/bin/bash
if [ -z "$1" ] & [ -z "$2" ] ; then
    echo Usage: $0 {domain} {pass}
    exit 1
fi

DOMAIN=$1
PASS=$2
DATE=`date +%d%m%Y%H%M%S`
USER=`/scripts/whoowns $DOMAIN`
PASSPATH=/home/$USER/etc/$DOMAIN/shadow
cp $PASSPATH{,.$DATE}

awk -F ":" '{print $1}' /home/$USER/etc/$DOMAIN/shadow | while read mailbox;
do
  uapi --output=jsonpretty --user=$USER Email passwd_pop email=$mailbox@$DOMAIN password=$PASS
done
