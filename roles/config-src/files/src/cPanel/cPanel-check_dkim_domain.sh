#!/bin/bash
#count=1
sed "s|:||g" /etc/userdomains | awk '!/^\* nobody/{print $1, $2 | "sort | uniq" }' > $me.account
#totalaccount=$(cat $me.account | wc -l)

cat $me.account | while read domain account
do
    #echo -n "Check - $account "
    #echo -n "- ($count/$totalaccount) "
    # Check Validate domains' DKIM records
    #count=$(($count+1))
    Status=$(whmapi1 --output=jsonpretty validate_current_dkims domain=$domain | jq '.data.payload | .[].state')
    echo -e "$domain - $Status"
    echo -n "--- Done ---"
done
exit 1
