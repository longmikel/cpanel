#!/bin/sh
#  * @Script description: Check archives mailing list
#  * @author     Mikel
#  * @url        https://matbao.com
#  * @license    GNU/GPL license: https://www.gnu.org/copyleft/gpl.html
#  * Created at: 2019/04/25
#  * Last modify: 2019/04/25
#  */s configuration script
# Use
if [ -z "$1" ]; then
  echo Usage: $0 {mailbox}
  exit 1
fi

# Check archives mailing list
echo "Preparing to check archives mailing list $1, please wait..."
sleep 3
i=`echo $1 | sed 's/@/_/g'`
# Check directory mailing list
if [ -d /usr/local/cpanel/3rdparty/mailman/archives/private/$i ]; then
  rm -rf /usr/local/cpanel/3rdparty/mailman/archives/private/$i/*
else
  echo -e "--- Mailing list $1 does not exist ---"
exit 1
fi

# Check file mailing list MBOX
if [ -f /usr/local/cpanel/3rdparty/mailman/archives/private/$i'.mbox'/$i'.mbox' ]; then 
  echo "" > /usr/local/cpanel/3rdparty/mailman/archives/private/$i'.mbox'/$i'.mbox'
else
  touch /usr/local/cpanel/3rdparty/mailman/archives/private/$i'.mbox'/$i'.mbox'
  chown -R mailman.mailman /usr/local/cpanel/3rdparty/mailman/archives/private/$i'.mbox'/$i'.mbox'
fi
echo -e "--- Done ---"

# Refresh mailing list
echo "Preparing to refresh mailing list $1, please wait..."
  /usr/local/cpanel/3rdparty/mailman/bin/arch --wipe $i
echo -e "--- Done ---"

# Permission mailing list
echo "Preparing to change owner and mod of mailing list $1, please wait..."
  chown -R mailman.mailman /usr/local/cpanel/3rdparty/mailman/archives/private/$i
  chown mailman.nobody /usr/local/cpanel/3rdparty/mailman/archives/private/$i
  chmod 710 /usr/local/cpanel/3rdparty/mailman/archives/private/$i
echo -e "--- Done ---"

# Update mailman cache
echo "Preparing to update mailman cache, please wait..."
  /scripts/update_mailman_cache
echo -e "--- Done ---"
exit 0
