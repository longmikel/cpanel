#!/bin/bash
# Use domain
if [ -z "$1" ]; then
    echo Usage: $0 {domain}
    exit 1
fi

# Check domain exist
echo "Preparing to check domain $1, please wait..."
sleep 3
i=`grep -E ^$1 /etc/userdomains | sed 's/://g' | awk '{print $2}'`
if [ -z $i ]; then
    echo -e "--- Domain $1 does not exist ---"
    exit 1
else
    echo -e "--- Domain $1 exist ---"
fi

# Get member for list
echo "Preparing to get member in list of domain $1, please wait..."
sleep 3
for list in `/usr/local/cpanel/3rdparty/mailman/bin/list_lists -b|grep $1`
do
    echo -e "Member for $list ##########   >_   ##########"
    /usr/local/cpanel/3rdparty/mailman/bin/list_members $list
done
exit 0
