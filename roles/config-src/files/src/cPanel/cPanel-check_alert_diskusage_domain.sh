#!/bin/bash
> /etc/snmp/scripts/over_quota.txt

PERCENT_ALERT=90

awk -F ": " '{print $1,$2}' /etc/trueuserdomains | grep -v salehost | while read DOMAIN USER;
do
    diskused=`whmapi1 accountsummary user=$USER | grep diskused | awk {'print$2'} | cut -d "M" -f1`
    disklimit=`whmapi1 accountsummary user=$USER | grep disklimit | awk {'print$2'} | cut -d "M" -f1`
    package=`grep -w $USER /etc/userplans|awk -F ": " '{print $2}'`
    percent=$(printf %.0f\\n "$(( 100 * $diskused / $disklimit ))e-0")
    hostname=`hostname`
    # if percent > 90%
    if [[ $percent -ge $PERCENT_ALERT ]];
        then
        limitint=`echo "$disklimit * 1" | bc`
        limit=`echo "$limitint * 1024*1024" | bc `
        usedint=`echo "$diskused * 1" | bc`
        used=`echo "$usedint * 1024*1024" | bc`
        echo -e "$hostname $DOMAIN $package $percent $limit $used" >> /etc/snmp/scripts/over_quota.txt
    fi
done;
