#!/bin/bash
if [ -z "$1" ] & [ -z "$2" ] ; then
    echo Usage: $0 {domain} {bakfile}
    exit 1
fi

DOMAIN=$1
USER=`/scripts/whoowns $DOMAIN`
BAKFILE=$2
PASSPATH=/home/$USER/etc/$DOMAIN/shadow
PASSPATH1=/home/$USER/etc/$DOMAIN/$BAKFILE
cp $PASSPATH1 $PASSPATH

#cp $PASSPATH1 /home/$USER/etc/$DOMAIN/shadow
chown $USER:$USER $PASSPATH
ls -al /home/$USER/etc/$DOMAIN/ | grep "shadow"
