#!/bin/bash
if [ -z "$1" ] ; then
    echo Usage: $0 {domain}
    exit 1
fi

DOMAIN=$1
DATE=`date +%d%m%Y%H%M%S`
USER=`/scripts/whoowns $DOMAIN`
PASSPATH=/home/$USER/etc/$DOMAIN/shadow
cp $PASSPATH{,.$DATE}
ls -al /home/$USER/etc/$DOMAIN/ | grep "shadow"
