#!/bin/bash
DATE=$(date +%Y-%m-%d)
HOSTNAME=$(hostname)
ALIAS_NAME=$(hostname -a)
BACKUP_DIR="/backup/$ALIAS_NAME"
SERVER_TARGET=""
IP=$(curl ipinfo.io/ip)
DOMAIN_TOTAL=$(find /var/cpanel/users ! -name "system" -type f -print | wc -l)
BACKUP_TOTAL_SERVER=$(ls -I transports.db $BACKUP_DIR | wc -l)
BACKUP_ACC_SERVER=$(find $BACKUP_DIR/$DATE/accounts -name "*.tar.gz" -type f -print | wc -l)
BACKUP_TOTAL_ONEDRIVE=$(rclone lsd backup:backup/cPanel/$ALIAS_NAME | wc -l)
BACKUP_ACC_ONEDRIVE=$(rclone lsf --exclude .master.meta --exclude ~tmp* backup:backup/cPanel/$ALIAS_NAME/$DATE/accounts | wc -l)

# Check Backup
[[ $BACKUP_ACC_SERVER -lt $DOMAIN_TOTAL ]] && STATUS="NOT OK" || STATUS="OK"

# Check Numbers Backup Miss on Server
if [[ $STATUS = "NOT OK" ]]; then
    NUMBERS_MISS=`expr $DOMAIN_TOTAL - $BACKUP_ACC_SERVER`
else
    NUMBERS_MISS=0
fi

# Create JSON string
echo "$HOSTNAME" - "$DOMAIN_TOTAL" - "$BACKUP_TOTAL_SERVER" - "$BACKUP_ACC_SERVER" - "$BACKUP_TOTAL_ONEDRIVE" - "$BACKUP_ACC_ONEDRIVE" - "$NUMBERS_MISS"| \
awk \
'{printf("{\
\"Server\": \"%s\", \
\"Total Domain\": %d, \
\"Total Backup on Server\": %d, \
\"Total Backup of each Account on Server\": %d, \
\"Total Backup on OneDrive\": %d, \
\"Total Backup of each Account on OneDrive\": %d, \
\"Numbers of Backup Miss\": %d}", \
$1,$3,$5,$7,$9,$11,$13)}' > /tmp/backupinfo.txt | scp -P1797 -oStrictHostKeyChecking=no -oCheckHostIP=no /tmp/backupinfo.txt $SERVER_TARGET:/usr/local/src/report/$HOSTNAME-backupinfo.txt
rm -rf /tmp/backupinfo.txt
exit 0
