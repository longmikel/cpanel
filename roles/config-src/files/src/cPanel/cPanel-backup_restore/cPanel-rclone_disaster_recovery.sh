#!/bin/bash
# Set verbose to null
verbose=""

## Set color
red=`tput setaf 1`
green=`tput setaf 2`
reset=`tput sgr0`
res1=$(date +%s.%N)
me=`basename "$0"`
count=1

# Print the help text
helptext () {
  tput bold
  tput setaf 2
  echo "[cPanel]"
  echo "USAGE: $0 [options]"
  echo "Options:"
  echo "  -h, --help  display this help and exit"
  echo "  -b, --backup specify account"
  echo "  -r, --recovery specify email"
  echo "."
  echo "."
  echo "."
  echo "- Backup & Restore Disaster Recovery"
  tput bold
  tput setaf 1
  echo "- Only run 1 time!"
  tput sgr0
  exit 0
}

# Sync backup/$alias_host to OneDrive by RClone
backup () {
  # Set variables
  ## Base
  alias_host="$(hostname -a)"
  date_current=$(date +%Y-%m-%d)
  date_delete=$(date --date='3 day ago' +%Y-%m-%d)
  
  ## Backup
  backup_path_local="/backup/$alias_host/$date_current"
  backup_path_remote="OneDriveRemote:Backup/cPanel/$alias_host"
  backup_path_remote_date_old=$(rclone lsd $backup_path_remote | awk '$5 ~ /'$date_delete'/' | awk '{print $5}')

  # Sync backup/$alias_host & Purge backup expire older than 3 day to OneDrive
  ## Sync all folder and content to OneDrive
  echo -n "Sync backup accounts on $alias_host to OneDrive "
    rclone sync $backup_path_local "$backup_path_remote/$date_current" >> /var/log/rclone.log 2>&1
  echo "- ${green}Done${reset}"

  ## Purge folder and content older than 3 day on OneDrive
  if [[ $backup_path_remote_date_old ]]; then
    echo -n "Purge backup accounts of $alias_host older than 3 day on OneDrive "
      rclone purge "$backup_path_remote/$date_delete" >> /var/log/rclone.log 2>&1
    echo "- ${green}Done${reset}"
  else
    echo -n "Backup accounts older than 3 day on OneDrive not does exits "
    echo "- ${green}Done${reset}"
  fi

  ###########   >_   #####################################################################
  res2=$(date +%s.%N)
  dt=$(echo "$res2 - $res1" | bc)
  dd=$(echo "$dt/86400" | bc)
  dt2=$(echo "$dt-86400*$dd" | bc)
  dh=$(echo "$dt2/3600" | bc)
  dt3=$(echo "$dt2-3600*$dh" | bc)
  dm=$(echo "$dt3/60" | bc)
  ds=$(echo "$dt3-60*$dm" | bc)
  echo "======================================"
  printf "Total runtime: %d:%02d:%02d:%02.4f\n" $dd $dh $dm $ds
  Total_runtime=$(printf "%d:%02d:%02d:%02.4f\n" $dd $dh $dm $ds)
  enddate=$(date +%T\ %d-%m-%Y)
  echo "=    $enddate    ="
  echo "======================================"

  ###########   >_   #####################################################################
  HOSTNAME=$(hostname -a)
  IP=$(curl ipinfo.io/ip)
  GROUP_ID=-4079011151
  BOT_TOKEN=6648163223:AAG7FPlGg1OCNMEJy37QEfYuY0ROEHsNdHo
  count_total_accounts=$(find /var/cpanel/users ! -name "system" -type f -print | wc -l)
  backup_count_accounts_local=$(rclone lsf --exclude .master.meta --exclude ~tmp* $backup_path_local/accounts | wc -l)
  backup_count_accounts_remote=$(rclone lsf --exclude .master.meta --exclude ~tmp* $backup_path_remote/$date_current/accounts | wc -l)
  TEXT_MESSAGE="
  Sync Backup to OneDrive for $HOSTNAME - Done
  ###########   >_   ###############################
  Tổng tài khoản trên máy chủ: $count_total_accounts
  Tổng tài khoản được sao lưu: $backup_count_accounts_local
  Tổng tài khoản được đồng bộ lên OneDrive: $backup_count_accounts_remote
  Tổng thời gian xử lý: $Total_runtime
  Thời gian kết thúc: $enddate
  "

  # Send mess telegram
  curl -s --data "text=$TEXT_MESSAGE" --data "chat_id=$GROUP_ID" 'https://api.telegram.org/bot'$BOT_TOKEN'/sendMessage' > /dev/null
}

recovery () {
  # Set variables
  alias_host="$(hostname -a)"
  date_current=$(date +%Y-%m-%d)
  backup_path_local="/backup/$alias_host/$date_current"
  backup_path_local_accounts="$backup_path_local/accounts"
  backup_path_local_system_files="$backup_path_local/system/files"
  backup_path_remote="OneDriveRemote:/Backup/cPanel/$alias_host"
  backup_count_accounts_remote=$(rclone lsf --exclude .master.meta --exclude ~tmp* $backup_path_remote/$date_current/accounts | wc -l)

  # Pull backup/$alias_host backup on OneDrive before recovery Server  
  rclone sync $backup_path_remote "/backup/$alias_host" >> /var/log/rclone.log 2>&1

  # Extract backup/$alias_host when pull on OneDrive
  find $backup_path_local_system_files -type f -name "*.gz" -exec gunzip {} \;

  # Convert backup/$alias_host after extract
  mv $backup_path_local_system_files/_etc_cpanel_exim_system_filter_mb /etc/cpanel_exim_system_filter_mb
  mv $backup_path_local_system_files/_etc_offlinedomains /etc/offlinedomains
  mv $backup_path_local_system_files/_etc_staticroutes /etc/staticroutes

  # Change owner for backup/$alias_host after convert
  chown -R root.mail /etc/offlinedomains
  chown -R root.mail /etc/staticroutes

  # Restore each accounts
  cd $backup_path_local_accounts
  cat /dev/null > $backup_path_local_accounts/list.txt
  ls *.tar.gz > $backup_path_local_accounts/list.txt
  totalaccount=$(cat $backup_path_local_accounts/list.txt | wc -l)

  ## Restore
  cat $backup_path_local_accounts/list.txt | while read account
  do
    echo -n "Restore - $account "
    echo -n "- ($count/$totalaccount) "
    count=$(($count+1))
    /scripts/restorepkg --force $account > $account.log
    echo "- ${green}Done${reset}"
  done
  echo "Total accounts: $totalaccount"

  ## Check restore
  ls *.log > $backup_path_local_accounts/log.txt
  cat $backup_path_local_accounts/log.txt | while read log
  do
    echo -n "$log >>>>>>>>>>>>>>>>>>>>>>>>>>>> "
    cat $log | grep "Account " | grep ": Success" || echo $log not restore 100%
  done > $backup_path_local_accounts/log_error.txt
  cat $backup_path_local_accounts/log_error.txt | grep "not restore"

  ###########   >_   #####################################################################
  res2=$(date +%s.%N)
  dt=$(echo "$res2 - $res1" | bc)
  dd=$(echo "$dt/86400" | bc)
  dt2=$(echo "$dt-86400*$dd" | bc)
  dh=$(echo "$dt2/3600" | bc)
  dt3=$(echo "$dt2-3600*$dh" | bc)
  dm=$(echo "$dt3/60" | bc)
  ds=$(echo "$dt3-60*$dm" | bc)
  echo "======================================"
  printf "Total runtime: %d:%02d:%02d:%02.4f\n" $dd $dh $dm $ds
  Total_runtime=$(printf "%d:%02d:%02d:%02.4f\n" $dd $dh $dm $ds)
  enddate=$(date +%T\ %d-%m-%Y)
  echo "=    $enddate    ="
  echo "======================================"

  ###########   >_   #####################################################################
  HOSTNAME=$(hostname -a)
  IP=$(curl ipinfo.io/ip)
  GROUP_ID=-4079011151
  BOT_TOKEN=6648163223:AAG7FPlGg1OCNMEJy37QEfYuY0ROEHsNdHo
  TEXT_MESSAGE="
  Recovery Accounts on [$HOSTNAME - $IP] - Done
  ###########   >_   ###########################
  Tổng tài khoản được phục hồi: $totalaccount/$backup_count_accounts_remote
  Tổng thời gian xử lý: $Total_runtime
  Thời gian kết thúc: $enddate
  "

  # Send mess telegram
  curl -s --data "text=$TEXT_MESSAGE" --data "chat_id=$GROUP_ID" 'https://api.telegram.org/bot'$BOT_TOKEN'/sendMessage' > /dev/null
}

# Main function, switches options passed to it
case "$1" in
  -h) helptext
  ;;
  --help) helptext

  case "$2" in
    --backup) backup "$3"
    ;;
    -b) backup "$3"
    ;;
    --recovery) recovery "$3"
    ;;
    -r) recovery "$3"
    ;;
    *)
    tput bold
    tput setaf 1
    echo "Invalid Option!"
    helptext
    ;;
  esac
    ;;
    --backup) backup "$2"
    ;;
    -b) backup "$2"
    ;;
    --recovery) recovery "$2"
    ;;
    -r) recovery "$2"
    ;;
    *)
    tput bold
    tput setaf 1
    echo "Invalid Option!"
    helptext
    ;;
esac
