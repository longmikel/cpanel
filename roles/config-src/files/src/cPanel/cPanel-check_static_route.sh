#!/bin/bash
ping1=`ping -c 5 192.168.49.254 | grep transmitted | awk '{print $4}'`
if [ $ping1 -eq 0 ]
    then
    route add -net 192.168.49.0/24 gw 192.168.48.254 eth1
fi

ping2=`ping -c 5 192.168.102.254 | grep transmitted | awk '{print $4}'`
if [ $ping2 -eq 0 ]
    then
    route add -net 192.168.102.0/24 gw 192.168.48.254 eth1
fi

ping3=`ping -c 5 192.168.113.254 | grep transmitted | awk '{print $4}'`
if [ $ping3 -eq 0 ]
    then
    route add -net 192.168.113.0/24 gw 192.168.48.254 eth1
fi
