#!/bin/bash
ls -all /home/*/mail/ | awk '$9 ~ /@/' | awk '{print $9, $11;}'| cut -d'/' -f1 > mailaccounts-link-webmail.txt
cat mailaccounts-link-webmail.txt | while read Email Domain
do
  Account="$(cat /etc/userdomains | grep "${Domain}" | cut -d' ' -f2)"
  echo -e "$Email - $Domain - $Account"
  DIR=/home/${Account}/mail/

if [ -d "${DIR}" ]
then
  cd ${DIR}
  unlink $Email
else
  echo -e "\e[1;31m\033[1m ERROR: \033[0m ${Email} does not exists."
  exit 1
fi
done
