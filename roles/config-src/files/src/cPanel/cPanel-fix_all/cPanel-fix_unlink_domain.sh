#!/bin/bash
if [ -z "$1" ]; then
   echo Usage: $0 {Account}
   exit 1
fi
ls -all /home/$1/mail/ | awk '$9 ~ /@/' | awk '{print $9, $11;}'| cut -d'/' -f1 > mailaccounts-link-webmail.txt
cat mailaccounts-link-webmail.txt | while read Email Domain
do
  echo -e "$Email - $Domain - $1"
  DIR=/home/$1/mail/

  if [ -d "${DIR}" ]
  then
    cd ${DIR}
    unlink $Email
  else
    echo -e "\e[1;31m\033[1m ERROR: \033[0m ${Email} does not exists."
  exit 1
  fi
done
