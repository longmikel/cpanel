#!/bin/bash
DATE=$(date '+%Y-%m-%d %H:%M:%S')

ls -1A /home/*/etc/*/*.rcube.db > rcube.db.txt
cat rcube.db.txt | while read list
do
    echo $list
    echo "backup db $list"
    cp $list{,.$DATE}
    echo "fix db"
    sqlite3 $list 'DELETE FROM identities WHERE identity_id = 2;'
    echo "Done."
done
