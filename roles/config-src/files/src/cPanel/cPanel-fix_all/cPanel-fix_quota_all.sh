#!/bin/bash
ls /home/*/etc/*/quota | cut -d'/' -f3 | sort -n > /tmp/account_quota_exit
cat /etc/userdomains | awk '{print $2}' | sort -n | sed '/nobody/d' > /tmp/account
diff -u /tmp/account_quota_exit /tmp/account | grep -E '^\+' | sed 's/^+//g' | sed '1d' > /tmp/account_quota_notfound

while IFS= read -r line
do
        echo -n "$line"
        domain=$(cat /etc/userdomains | grep $line | cut -d: -f1)
        touch /home/$line/etc/$domain/quota
        echo -n " ... "
        chown $line.mail /home/$line/etc/$domain/quota
        echo "Done"
done < /tmp/account_quota_notfound
