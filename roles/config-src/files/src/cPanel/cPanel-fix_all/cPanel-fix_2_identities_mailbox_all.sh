#!/bin/bash
ls -1A /home/*/etc/*/*.rcube.db > rcube.db.txt
cat rcube.db.txt | while read list
do
  echo $list
  echo "backup db $list"
  cp $list{,.backup}
  echo "fix db"
  sqlite3 $list 'DELETE FROM identities WHERE identity_id = 2;'
echo "Done."
done
