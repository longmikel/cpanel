#!/bin/bash
## List domain in domainuser when symlink for users
cat /etc/domainusers | awk '{print $1}' | sed 's/://g' | while read USER; do symlink=/home/$USER/public_html/index.html

if [ -e "$symlink" ]; then
  if [ ! -L "$symlink" ]; then
    echo "File index.html in $USER is not a symlink and will be made a symlink"
    ln -sf /usr/local/src/cPanel/cPanel-uri_webmail/index.html $symlink
  else
    echo "File index.html in $USER is available and is a symlink"
  fi
  else
    echo "File index.html in $USER is unavailable and will be created"
    ln -sf /usr/local/src/cPanel/cPanel-uri_webmail/index.html $symlink
fi
done
