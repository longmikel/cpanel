#!/bin/bash
res1=$(date +%s.%N)
# Set verbose to null
verbose=""

# Print the help text
helptext () {
    echo "[cPanel]"
    echo "USAGE: $0 [Options] [Domain] [Server]"
    echo "Options:"
    echo "  -h, --help     display this help and exit"
    echo "  -d, --domain   select one domain will rsync data from source server old"
    echo "."
    echo "."
    echo "."
        echo "- Only run 1 time!"
    exit 0
}

domain () {
###########   >_   #####################################################################
# Rsync
domain=$1
target_server=$2
account=`grep $domain /etc/domainusers | awk -F ":" '{print $1}'`
x_private_key="/root/.ssh/id_migrate"

rsync -avP -e "ssh -p1797 -i $x_private_key -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no" /home/$account/ root@$target_server:/home/$account/ > /dev/null 2>&1

###########   >_   #####################################################################
res2=$(date +%s.%N)
dt=$(echo "$res2 - $res1" | bc)
dd=$(echo "$dt/86400" | bc)
dt2=$(echo "$dt-86400*$dd" | bc)
dh=$(echo "$dt2/3600" | bc)
dt3=$(echo "$dt2-3600*$dh" | bc)
dm=$(echo "$dt3/60" | bc)
ds=$(echo "$dt3-60*$dm" | bc)
###########   >_   #####################################################################
printf "Total runtime: %d:%02d:%02d:%02.4f\n" $dd $dh $dm $ds
Total_runtime=$(printf "%d:%02d:%02d:%02.4f\n" $dd $dh $dm $ds)
enddate=$(date +%T\ %d-%m-%Y)
echo "End date: $enddate"

###########   >_   #####################################################################
HOSTNAME=$(hostname)

# Webhook or Token.
WEBHOOK_URL="https://axysasia.webhook.office.com/webhookb2/4afc8345-87e4-48c8-bfc2-964a2f590585@58bc40e1-9e84-4da4-93a6-de5aa4ef21de/IncomingWebhook/8049f263d28849a89b748e8301cb75a9/346b1119-1344-4b48-9f69-ea9c24dab4a2"
TITLE="Rsync Data for Domain $domain from $server to $HOSTNAME"
TEXT="The data synchronization process is complete within the total processing time: $Total_runtime - $enddate"

# Convert formating.
MESSAGE=$( echo ${TEXT} | sed 's/"/\"/g' | sed "s/'/\'/g" )
JSON="{\"title\": \"${TITLE}\", \"text\": \"${MESSAGE}\" }"

# Post to Microsoft Teams.
curl -H "Content-Type: application/json" -d "${JSON}" "${WEBHOOK_URL}"

# Clean up backup config for domain after sync data
ssh -T -p1797 -i $x_private_key root@$target_server -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no "rm -rf /home/ubuntu/$account.tar.gz"

# Out session Tmux after sync data
_domain=$account
tmux kill-session -t $_domain
}

# Main function, switches options passed to it
case "$1" in
    -h) helptext
    ;;
    --help) helptext

    case "$2" in
        --domain|-d) 
        domain "$3" "$4"
        ;;
    *)
        echo "Invalid Option!"
        helptext
        ;;
    esac
        ;;
        --domain|-d)
        if [ -z $2 ] || [ -z $3 ]; then
            echo "Invalid Option! Missing arguments for domain or server."
            helptext
            exit 1
        elif  [ $# -gt 3 ]; then
            echo "Too many arguments, should only add 1 domain and 1 server argument."
            helptext
            exit 1
        else
        domain "$2" "$3"
        fi
        ;;
    *)
        echo "Invalid Option!"
        helptext
        ;;                
esac