#!/bin/bash
res1=$(date +%s.%N)

# Set verbose to null
verbose=""

# Print the help text
helptext () {
    echo "[cPanel]"
    echo "USAGE: $0 [Options] [Server] or [Options] [Domain] [Server]"
    echo "Options:"
    echo "  -h, --help     display this help and exit"
    echo "  -d, --domain   select one domain will backup config"
    echo "."
    echo "."
    echo "."
        echo "- Only run 1 time!"
    exit 0
}

# Define html
echo "<!DOCTYPE html>
<html lang="vi">
<head>
    <meta charset="UTF-8">
    <title>Hiển thị thông tin màu sắc</title>
    <style>
        /* Định dạng chung */
        body {
            font-family: Arial, sans-serif;
        }

        /* Định dạng cho thông báo */
        .check {
            color: purple; /* Màu tím cho phần kiểm tra */
            font-weight: bold; /* Tăng độ đậm cho phần CHECK */
        }
        .info {
            color: blue; /* Màu xanh dương cho phần thông tin */
            font-weight: bold; /* Tăng độ đậm cho phần INFO */
        }
        .waiting {
            color: orange; /* Màu cam cho phần chờ */
            font-weight: bold; /* Tăng độ đậm cho phần WAITING */
        }
        .error {
            color: red; /* Màu đỏ cho phần lỗi */
            font-weight: bold; /* Tăng độ đậm cho phần ERROR */
        }
    </style>
</head>
</html>
"

domain () {
###########   >_   #####################################################################
# Define params
domain=$1
source_server=$(hostname)
target_server=$2
account=`grep $domain /etc/domainusers | awk -F ":" '{print $1}'`
x_tools="/usr/local/src/cPanel"
x_backup_path_domain="/home/$account"
x_private_key="/root/.ssh/id_migrate"                 

###########   >_   #####################################################################
# Check account
echo "<!DOCTYPE html>
<html lang="vi">
    <body>
        <p><span class="check">[CHECK]</span> - Check domain $domain, please wait...</p>
    </body>
</html>
"
## Check account on server before backup
if [ -z $account ]; then
    echo -e "$domain does not exists on server"
    exit 1
fi

# Running backup
echo "<!DOCTYPE html>
<html lang="vi">
    <body>
        <p><span class="info">[INFO]</span> - Running Backup domain $domain, please wait...</p>
    </body>
</html>
"
# Running backup and copy to target_source
sleep 3
## Running backup config for domain on source_server
/scripts/pkgacct $account --backup --skiplogs "$x_backup_path_domain" > /dev/null 2>&1
## Running copy config for domain from source_server to target_server
scp -i $x_private_key -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -P1797 $x_backup_path_domain/$account.tar.gz root@$target_server:/home/ubuntu/ > /dev/null 2>&1

###########   >_   #####################################################################
sleep 3
if [ -f $x_backup_path_domain/$account.'tar.gz' ]; then
    echo "<!DOCTYPE html>
    <html lang="vi">
        <body>
            <p><span class="info">[INFO]</span> - Running Restore for Domain $domain, please wait...</p>
        </body>
    </html>
    "
    ## Running restore config for domain on target_server
    sleep 3
    ssh -T -p1797 -i $x_private_key root@$target_server -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no "/scripts/restorepkg --force /home/ubuntu/$account.tar.gz" > /dev/null 2>&1
else
    echo "<!DOCTYPE html>
    <html lang="vi">
        <body>
            <p><span class="error">[ERROR]</span> - File backup config for Domain $domain does not exists on the server</p>
        </body>
    </html>
    "
    exit 1
fi

###########   >_   #####################################################################
echo "<!DOCTYPE html>
<html lang="vi">
    <body>
        <p><span class="info">[INFO]</span> - Running Rsync Data for Domain $domain from $source_server to $target_server, please wait...</p>
    </body>
</html>
"
## Running rsync data for domain from source_server to target_server
_domain=$account
tmux new-session -d -s $_domain && tmux send-keys -t $_domain "/usr/local/src/cPanel/cPanel-migrate-cPanel/cPanel-transfer_tool_rsync.sh -d $domain $target_server" Enter

###########   >_   #####################################################################
echo "<!DOCTYPE html>
<html lang="vi">
    <body>
        <p><span class="waiting">[WAITING]</span> - Migrate $domain from $source_server to $target_server is running in the background, please wait... for notification when the process is complete</p>
    </body>
</html>
"
}
###########   >_   #####################################################################

# Main function, switches options passed to it
case "$1" in
    -h) helptext
    ;;
    --help) helptext

    case "$2" in
        --domain|-d) 
        domain "$3" "$4"
        ;;
    *)
        echo "Invalid Option!"
        helptext
        ;;
    esac
        ;;
        --domain|-d)
        if [ -z $2 ] || [ -z $3 ]; then
            echo "Invalid Option! Missing arguments for domain or server."
            helptext
            exit 1
        elif  [ $# -gt 3 ]; then
            echo "Too many arguments, should only add 1 domain and 1 server argument."
            helptext
            exit 1
        else
            domain "$2" "$3"
        fi
        ;;
    *)
        echo "Invalid Option!"
        helptext
        ;;                
esac