#!/bin/bash
red=`tput setaf 1`
green=`tput setaf 2`
reset=`tput sgr0`
res1=$(date +%s.%N)
me=`basename "$0"`
count=1

###########   >_   #####################################################################
# Restore config
cd /home/ubuntu
cat /dev/null > /tmp/list.txt
ls *.tar.gz > /tmp/list.txt
totalaccount=$(cat /tmp/list.txt | wc -l)

cat /tmp/list.txt | while read account
do
    echo -n "Restore - $account "
    echo -n "- ($count/$totalaccount) "
    count=$(($count+1))
    /scripts/restorepkg --force $account > $account.log
    echo "- ${green}Done${reset}"
done
echo "Total accounts: $totalaccount"

# Check restore
ls *.log > log.txt
cat log.txt | while read log
do
    echo -n "$log >>>>>>>>>>>>>>>>>>>>>>>>>>>> "
    cat $log | grep "Account " | grep ": Success" || echo $log not restore 100%
done > log_error.txt
cat log_error.txt | grep "not restore"

###########   >_   #####################################################################
res2=$(date +%s.%N)
dt=$(echo "$res2 - $res1" | bc)
dd=$(echo "$dt/86400" | bc)
dt2=$(echo "$dt-86400*$dd" | bc)
dh=$(echo "$dt2/3600" | bc)
dt3=$(echo "$dt2-3600*$dh" | bc)
dm=$(echo "$dt3/60" | bc)
ds=$(echo "$dt3-60*$dm" | bc)
echo "======================================"
printf "Total runtime: %d:%02d:%02d:%02.4f\n" $dd $dh $dm $ds
Total_runtime=$(printf "%d:%02d:%02d:%02.4f\n" $dd $dh $dm $ds)
enddate=$(date +%T\ %d-%m-%Y)
echo "=    $enddate    ="
echo "======================================"

###########   >_   #####################################################################
HOSTNAME=$(hostname -a)
IP=$(curl ipinfo.io/ip)
GROUP_ID=-392556851
BOT_TOKEN=799602484:AAH_SDpbOkrrWEJHaQp9oZN0b9j--vQumpE
TEXT_MESSAGE="
Restore Accounts on [$HOSTNAME - $IP] - Done
###########   >_   ##################
Tổng thời gian xử lý: $Total_runtime
Thời gian kết thúc: $enddate
"

# Send mess telegram
curl -s --data "text=$TEXT_MESSAGE" --data "chat_id=$GROUP_ID" 'https://api.telegram.org/bot'$BOT_TOKEN'/sendMessage' > /dev/null
