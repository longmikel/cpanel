#!/bin/bash
if [ -z "$1" ]; then
    echo ============= Usage: $0 xx
    exit 1
fi

red=`tput setaf 1`
green=`tput setaf 2`
reset=`tput sgr0`
res1=$(date +%s.%N)
me=`basename "$0"`
count=1

###########   >_   #####################################################################
# Fix file quota if not exits
ls /home/*/etc/*/quota | cut -d'/' -f3 | sort -n > /tmp/account_quota_exit
cat /etc/userdomains | awk '{print $2}' | sort -n | sed '/nobody/d' > /tmp/account
diff -u /tmp/account_quota_exit /tmp/account | grep -E '^\+' | sed 's/^+//g' | sed '1d' > /tmp/account_quota_notfound

while IFS= read -r line
do
    echo -n "$line"
    domain=$(cat /etc/userdomains | grep $line | cut -d: -f1)
    touch /home/$line/etc/$domain/quota
    echo -n " ... "
    chown $line.mail /home/$line/etc/$domain/quota
    echo "Done"
done < /tmp/account_quota_notfound

# Create backup
mkdir -p /backup/
rm -rf /backup/*.tar.gz

cat /etc/userdomains | sed '/: nobody/d' | awk {'print $2'} | sort > $me.account
totalaccount=$(cat $me.account | wc -l)

# Backup to folder /backup
cat $me.account | while read account
do
    echo -n "Backup - $account "
    echo -n "- ($count/$totalaccount) "
    count=$(($count+1))
    /scripts/pkgacct --backup --skiplogs $account /backup/ > /dev/null
    echo "- ${green}Done${reset}"
done
echo "Total accounts: $totalaccount"

###########   >_   #####################################################################
# Scp to new server
echo "SCP Coping ..........................."
scp -P1797 /backup/*.gz root@$1:/home/ubuntu
echo "Copy Backup Accounts to $1 - Done"
echo "======================================"

###########   >_   #####################################################################
res2=$(date +%s.%N)
dt=$(echo "$res2 - $res1" | bc)
dd=$(echo "$dt/86400" | bc)
dt2=$(echo "$dt-86400*$dd" | bc)
dh=$(echo "$dt2/3600" | bc)
dt3=$(echo "$dt2-3600*$dh" | bc)
dm=$(echo "$dt3/60" | bc)
ds=$(echo "$dt3-60*$dm" | bc)
echo "======================================"
printf "Total runtime: %d:%02d:%02d:%02.4f\n" $dd $dh $dm $ds
Total_runtime=$(printf "%d:%02d:%02d:%02.4f\n" $dd $dh $dm $ds)
enddate=$(date +%T\ %d-%m-%Y)
echo "=    $enddate    ="
echo "======================================"

###########   >_   #####################################################################
HOSTNAME=$(hostname -a)
IP=$(curl ipinfo.io/ip)
GROUP_ID=-392556851
BOT_TOKEN=799602484:AAH_SDpbOkrrWEJHaQp9oZN0b9j--vQumpE
TEXT_MESSAGE="
Backup Accounts to [$1] - Done
###########   >_   ##################
Tổng thời gian xử lý: $Total_runtime
Thời gian kết thúc: $enddate
"

# Send mess telegram
curl -s --data "text=$TEXT_MESSAGE" --data "chat_id=$GROUP_ID" 'https://api.telegram.org/bot'$BOT_TOKEN'/sendMessage' > /dev/null
