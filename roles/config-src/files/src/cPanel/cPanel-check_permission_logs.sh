#!/bin/bash
path_access="/usr/local/cpanel/logs/access_log"
path_api="/usr/local/cpanel/logs/api_log"
permission="-rw-------"

# Find permission for logs access and api files
i_access=`ls -l $path_access| awk '{print $1}'`
i_api=`ls -l $path_api| awk '{print $1}'`

# Check permission for logs access 
if [ $i_access = $permission ]; then
    chmod 644 /usr/local/cpanel/logs/access_log
fi

# Check permission for logs api
if [ $i_api = $permission ]; then
    chmod 644 /usr/local/cpanel/logs/api_log
fi