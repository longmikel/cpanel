#!/bin/bash
# Set verbose to null
verbose=""

# Print the help text
helptext () {
  echo "[cPanel]"
  echo "USAGE: $0 [options]"
  echo "Options:"
  echo "  -h, --help  display this help and exit"
  echo "  -rdm, --reset-database-email reset database for email"
  echo "."
  echo "."
  echo "."
  echo "- Email Account"
  echo "- Only run 1 time!"
  exit 0
}

reset_database_email () {
  # Check mailbox parameters are passed
    if [ -z "$1" ]; then
    echo Usage: $0 [option] {email}
    echo "  -rdm, --reset-database-email reset database for email"
    echo "."
      echo "- Reset Database for Email"
    exit 1
  fi
  # Check mailbox existence in limit setting
  email=$1
  user="$(echo "${email}" | cut -d'@' -f1)"
  domain="$(echo "${email}" | cut -d'@' -f2)"
  account="$(cat /etc/userdomains | grep "${domain}" | cut -d' ' -f2)"
  rcube=/home/${account}/etc/${domain}/${user}'.rcube.db'
  date=`date +%d%m%Y%H%M%S`

  # Check email
  if [ -f "${rcube}" ]
  then
    echo ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> $email"
    mv ${rcube} ${rcube}.$date
  else
      echo "ERROR: ${email} does not exists."
      exit 1
  fi
}

# Main function, switches options passed to it
case "$1" in
    -h) helptext
    ;;
    --help) helptext

    case "$2" in
    -rdm) reset_database_email "$3"
    ;;
      *)
      echo "Invalid Option!"
      helptext
      ;;
    esac
    ;;
    -rdm) reset_database_email $2
    ;;
      *)
      echo "Invalid Option!"
      helptext
      ;;
esac
