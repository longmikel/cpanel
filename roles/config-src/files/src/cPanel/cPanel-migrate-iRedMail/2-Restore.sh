#!/bin/bash

red=`tput setaf 1`
green=`tput setaf 2`
yellow=`tput setaf 3`
blue=`tput setaf 4`
reset=`tput sgr0`
res1=$(date +%s.%N)
me=`basename "$0"`
count=1
###########   >_   #####################################################################

# Set verbose to null
verbose=""

# Print the help text
helptext () {
    tput bold
    tput setaf 2
    echo "[iRedMail]"
    tput bold
    tput setaf 3
    echo "USAGE: $0 [Options] or [Options] [Domain]"
    tput bold
    tput setaf 4
    echo "Options:"
    echo "  -h, --help     display this help and exit"
    echo "  -a, --all      run restore config for all domains on server"
    echo "  -d, --domain   select one domain will restore config"
    echo "."
    echo "."
    echo "."
        tput bold
        tput setaf 1
        echo "- Only run 1 time!"
    tput sgr0
    exit 0
}

helptext_all () {
    tput bold
    tput setaf 2
    echo "[iRedMail]"
    tput bold
    tput setaf 3
    echo "USAGE: $0 [Options]"
    tput bold
    tput setaf 4
    echo "Options:"
    echo "  -h, --help     display this help and exit"
    echo "  -a, --all      run restore config for all domains on server"
    echo "."
    echo "."
    echo "."
        tput bold
        tput setaf 1
        echo "- Only run 1 time!"
    tput sgr0
    exit 0
}

helptext_domain () {
    tput bold
    tput setaf 2
    echo "[iRedMail]"
    tput bold
    tput setaf 3
    echo "USAGE: $0 [Options] [Domain]"
    tput bold
    tput setaf 4
    echo "Options:"
    echo "  -h, --help     display this help and exit"
    echo "  -d, --domain   select one domain will restore config"
    echo "."
    echo "."
    echo "."
        tput bold
        tput setaf 1
        echo "- Only run 1 time!"
    tput sgr0
    exit 0
}

all () {
###########   >_   #####################################################################
# Restore
cd /home/ubuntu/iredmail
cat /dev/null > list.txt
ls *.tar.gz > list.txt
x_total=$(cat list.txt | wc -l)

echo "[ ${blue}INFO${reset} ] Running Restore All Domains"
cat list.txt | sed 's/.tar.gz//g' | while read domain
do
    echo -n "Restore - $domain "
    echo -n "- ($count/$x_total) "
    count=$(($count+1))

    # Define variables
    x_path="/home/ubuntu/iredmail/$domain"
    
    x_tools="/usr/local/src/iRedMail"

    x_binddn="cn=Manager,dc=emailserver,dc=vn"

    x_basedn="o=domains,dc=emailserver,dc=vn"
    
    x_bindpw="4maYJNQGvpMyU4uxIU4HaMvi7K7sPgMH"
    
    # Uncompress file backup for domain
    tar xvf $domain.tar.gz > /dev/null

    x_domain=$(ldapsearch -x -o ldif-wrap=no -D $x_binddn -w $x_bindpw -b $x_basedn domainName | awk '/domainName:/ {print $2}' | awk "/$domain/")
    if [ -z "$x_domain" ]
    then
        # Create domain
        python3 $x_tools/scripts/iRedMail-create_domain.py $domain > /dev/null
    fi

    # Manage user for domain
    ## Import user for domain to become Admin
    x_admin=$(find $x_path -iname "export-mailboxes_information_admin*")
    if [ -n "$x_admin" ]
    then
        # Generates file *.ldif export mailbox without forward
        python3 $x_tools/scripts/iRedMail-create_mailboxes_OpenLDAP_domainadmin.py $x_path/export-mailboxes_information_admin.$domain.csv > /dev/null
        ## Import user from file *.ldif by ldapadd
        x_admin_ldif=$(find $x_path -iname "export-mailboxes_information_admin.$domain.csv.ldif")
        if [ -n "$x_admin_ldif" ]
        then
            # Import user admin
            ldapadd -x -D $x_binddn -w $x_bindpw -f $x_path/export-mailboxes_information_admin.$domain.csv.ldif > /dev/null
            # Become an admin for the domain
            ldapmodify -x -D $x_binddn -w $x_bindpw -f $x_path/export-mailboxes_information_admin.$domain.csv.admin.ldif > /dev/null
        fi
    fi

    ## Import user for domain not have forwarded
    x_none_fw=$(find $x_path -iname "export-mailboxes_information_none_forward*")
    if [ -n "$x_none_fw" ]
    then
        # Generates file *.ldif export mailbox without forward
        python3 $x_tools/scripts/iRedMail-create_mailboxes_OpenLDAP_none_forward.py $x_path/export-mailboxes_information_none_forward.$domain.csv > /dev/null
        ## Import user from file *.ldif by ldapadd
        x_none_fw_ldif=$(find $x_path -iname "export-mailboxes_information_none_forward.$domain.csv.ldif")
        if [ -n "$x_none_fw_ldif" ]
        then
            ldapadd -x -D $x_binddn -w $x_bindpw -f $x_path/export-mailboxes_information_none_forward.$domain.csv.ldif > /dev/null
        fi
    fi

    ## Import user for domain have forwarded
    x_fw=$(find $x_path -iname "export-mailboxes_information_forward*")
    if [ -n "$x_fw" ]
    then
        # Generates file *.ldif export mailbox with forward
        python3 $x_tools/scripts/iRedMail-create_mailboxes_OpenLDAP_forward.py $x_path/export-mailboxes_information_forward.$domain.csv > /dev/null
        # Import user from file *.ldif by ldapadd
        x_fw_ldif=$(find $x_path -iname "export-mailboxes_information_none_forward.$domain.csv.ldif")
        if [ -n "$x_fw_ldif" ]
        then
            ldapadd -x -D $x_binddn -w $x_bindpw -f $x_path/export-mailboxes_information_forward.$domain.csv.ldif > /dev/null
        fi
    fi

    # Manage mailing lis
    x_list=$(find $x_path -iname "export-list_*")
    if [ -n "$x_list" ]
    then
    ls $x_list | sed 's/export-list_//g' | awk -F "/" '{print $6}' | while read list
    do
        ## Create mailing list
        python3 /opt/mlmmjadmin/tools/maillist_admin.py create $list disable_archive=no > /dev/null
        cat $x_path/export-list_$list | while read subscriber
        do
            ## Import subscribers to list
            python3 /opt/mlmmjadmin/tools/maillist_admin.py add_subscribers $list $subscriber > /dev/null
        done
    done
    fi
    echo "- ${green}Done${reset}"
done
echo "----------------------------------------------------------------------------------"

# Return overview's information
echo "[ ${yellow}INFO${reset} ] Return Overview's Information after Restore"
echo "Domain Total: ${red}$x_total${reset}"
echo "----------------------------------------------------------------------------------"

###########   >_   #####################################################################
res2=$(date +%s.%N)
dt=$(echo "$res2 - $res1" | bc)
dd=$(echo "$dt/86400" | bc)
dt2=$(echo "$dt-86400*$dd" | bc)
dh=$(echo "$dt2/3600" | bc)
dt3=$(echo "$dt2-3600*$dh" | bc)
dm=$(echo "$dt3/60" | bc)
ds=$(echo "$dt3-60*$dm" | bc)
###########   >_   #####################################################################
echo "[ ${green}SUCCESS${reset} ] Restore Completed"
printf "Total runtime: %d:%02d:%02d:%02.4f\n" $dd $dh $dm $ds
Total_runtime=$(printf "%d:%02d:%02d:%02.4f\n" $dd $dh $dm $ds)
enddate=$(date +%T\ %d-%m-%Y)
echo "End date: $enddate"

###########   >_   #####################################################################
HOSTNAME=$(hostname)
IP=$(ip -4 addr show dev eth0 | sed -ne 's|^.* inet \([^/]*\)/.* scope global.*$|\1|p')
GROUP_ID=-1002041996431
BOT_TOKEN=6343370300:AAFMcxiZQdtQLJSB_dt-0XAPld6jd-r1jcc
TEXT_MESSAGE="
Restore Config for All Domains on [$HOSTNAME - $IP] - Done
###########   >_   ##################
Tổng thời gian xử lý: $Total_runtime
Thời gian kết thúc: $enddate
"

# Send mess telegram
curl -s --data "text=$TEXT_MESSAGE" --data "chat_id=$GROUP_ID" 'https://api.telegram.org/bot'$BOT_TOKEN'/sendMessage' > /dev/null
}

domain () {
###########   >_   #####################################################################
# Restore
domain=$1
cd /home/ubuntu/iredmail

echo -n "[ ${blue}INFO${reset} ] Running Restore for Domain $domain "

# Define variables
x_path="/home/ubuntu/iredmail/$domain"

x_tools="/usr/local/src/iRedMail"

x_binddn="cn=Manager,dc=emailserver,dc=vn"

x_basedn="o=domains,dc=emailserver,dc=vn"

x_bindpw="4maYJNQGvpMyU4uxIU4HaMvi7K7sPgMH"

# Uncompress file backup for domain
tar xvf $domain.tar.gz > /dev/null

x_domain=$(ldapsearch -x -o ldif-wrap=no -D $x_binddn -w $x_bindpw -b $x_basedn domainName | awk '/domainName:/ {print $2}' | awk "/$domain/")
if [ -z "$x_domain" ]
then
    # Create domain
    python3 $x_tools/scripts/iRedMail-create_domain.py $domain > /dev/null
fi

# Manage user for domain
## Import user for domain to become Admin
x_admin=$(find $x_path -iname "export-mailboxes_information_admin*")
if [ -n "$x_admin" ]
then
    # Generates file *.ldif export mailbox without forward
    python3 $x_tools/scripts/iRedMail-create_mailboxes_OpenLDAP_domainadmin.py $x_path/export-mailboxes_information_admin.$domain.csv > /dev/null
    ## Import user from file *.ldif by ldapadd
    x_admin_ldif=$(find $x_path -iname "export-mailboxes_information_admin.$domain.csv.ldif")
    if [ -n "$x_admin_ldif" ]
    then
        # Import user admin
        ldapadd -x -D $x_binddn -w $x_bindpw -f $x_path/export-mailboxes_information_admin.$domain.csv.ldif > /dev/null
        # Become an admin for the domain
        ldapmodify -x -D $x_binddn -w $x_bindpw -f $x_path/export-mailboxes_information_admin.$domain.csv.admin.ldif > /dev/null
    fi
fi

## Import user for domain not have forwarded
x_none_fw=$(find $x_path -iname "export-mailboxes_information_none_forward*")
if [ -n "$x_none_fw" ]
then
    # Generates file *.ldif export mailbox without forward
    python3 $x_tools/scripts/iRedMail-create_mailboxes_OpenLDAP_none_forward.py $x_path/export-mailboxes_information_none_forward.$domain.csv > /dev/null
    ## Import user from file *.ldif by ldapadd
    x_none_fw_ldif=$(find $x_path -iname "export-mailboxes_information_none_forward.$domain.csv.ldif")
    if [ -n "$x_none_fw_ldif" ]
    then
        ldapadd -x -D $x_binddn -w $x_bindpw -f $x_path/export-mailboxes_information_none_forward.$domain.csv.ldif > /dev/null
    fi
fi

## Import user for domain have forwarded
x_fw=$(find $x_path -iname "export-mailboxes_information_forward*")
if [ -n "$x_fw" ]
then
    # Generates file *.ldif export mailbox with forward
    python3 $x_tools/scripts/iRedMail-create_mailboxes_OpenLDAP_forward.py $x_path/export-mailboxes_information_forward.$domain.csv > /dev/null
    # Import user from file *.ldif by ldapadd
    x_fw_ldif=$(find $x_path -iname "export-mailboxes_information_none_forward.$domain.csv.ldif")
    if [ -n "$x_fw_ldif" ]
    then
        ldapadd -x -D $x_binddn -w $x_bindpw -f $x_path/export-mailboxes_information_forward.$domain.csv.ldif > /dev/null
    fi
fi

# Manage mailing lis
x_list=$(find $x_path -iname "export-list_*")
if [ -n "$x_list" ]
then
ls $x_list | sed 's/export-list_//g' | awk -F "/" '{print $6}' | while read list
do
    ## Create mailing list
    python3 /opt/mlmmjadmin/tools/maillist_admin.py create $list disable_archive=no > /dev/null
    cat $x_path/export-list_$list | while read subscriber
    do
        ## Import subscribers to list
        python3 /opt/mlmmjadmin/tools/maillist_admin.py add_subscribers $list $subscriber > /dev/null
    done
done
fi
echo "- ${green}Done${reset}"
echo "----------------------------------------------------------------------------------"

###########   >_   #####################################################################
res2=$(date +%s.%N)
dt=$(echo "$res2 - $res1" | bc)
dd=$(echo "$dt/86400" | bc)
dt2=$(echo "$dt-86400*$dd" | bc)
dh=$(echo "$dt2/3600" | bc)
dt3=$(echo "$dt2-3600*$dh" | bc)
dm=$(echo "$dt3/60" | bc)
ds=$(echo "$dt3-60*$dm" | bc)
###########   >_   #####################################################################
echo "[ ${green}SUCCESS${reset} ] Restore Completed"
printf "Total runtime: %d:%02d:%02d:%02.4f\n" $dd $dh $dm $ds
Total_runtime=$(printf "%d:%02d:%02d:%02.4f\n" $dd $dh $dm $ds)
enddate=$(date +%T\ %d-%m-%Y)
echo "End date: $enddate"

###########   >_   #####################################################################
HOSTNAME=$(hostname)
IP=$(ip -4 addr show dev eth0 | sed -ne 's|^.* inet \([^/]*\)/.* scope global.*$|\1|p')
GROUP_ID=-1002041996431
BOT_TOKEN=6343370300:AAFMcxiZQdtQLJSB_dt-0XAPld6jd-r1jcc
TEXT_MESSAGE="
Restore Config for Domain $domain on $HOSTNAME - Done
###########   >_   ##################
Tổng thời gian xử lý: $Total_runtime
Thời gian kết thúc: $enddate
"

# Send mess telegram
curl -s --data "text=$TEXT_MESSAGE" --data "chat_id=$GROUP_ID" 'https://api.telegram.org/bot'$BOT_TOKEN'/sendMessage' > /dev/null
}

# Main function, switches options passed to it
case "$1" in
    -h) helptext
    ;;
    --help) helptext

    case "$2" in
        --all|-a) 
        all
        ;;
        --domain|-d)
        domain "$3"
        ;;
    *)
        tput bold
        tput setaf 1
        echo "Invalid Option!"
        helptext
        ;;
    esac
        ;;
        --all|-a)
        if [ -z $1 ]; then
            tput bold
            tput setaf 1
            echo "Invalid Option! Missing select options --domain or -d."
            helptext_all
            exit 1
        elif  [ $# -gt 1 ]; then
            tput bold
            tput setaf 1
            echo "Too many arguments, should only add options --domain or -d."
            helptext_all
            exit 1
        else
            all
        fi
        ;;
        --domain|-d)
        if [ -z $2 ]; then
            tput bold
            tput setaf 1
            echo "Invalid Option! Missing arguments for domain."
            helptext_domain
            exit 1
        elif  [ $# -gt 2 ]; then
            tput bold
            tput setaf 1
            echo "Too many arguments, should only add 1 domain argument."
            helptext_domain
            exit 1
        else
        domain "$2"
        fi
        ;;
    *)
        tput bold
        tput setaf 1
        echo "Invalid Option!"
        helptext
        ;;                
esac