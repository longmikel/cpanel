#!/bin/bash
# Set verbose to null
verbose=""

# Print the help text
helptext () {
    echo "[iRedMail]"
    echo "USAGE: $0 [Options] [Domain] [Server]"
    echo "Options:"
    echo "  -h, --help     display this help and exit"
    echo "  -d, --domain   select one domain will backup config"
    echo "."
    echo "."
    echo "."
        echo "- Only run 1 time!"
    exit 0
}

domain () {
###########   >_   #####################################################################
# Define variables
domain=$1
server=$2

# Check domain quota before backup config
account="$(cat /etc/userdomains | grep "${domain}" | cut -d' ' -f2)"
ls /home/${account}/etc/${domain}/quota | cut -d'/' -f3 | sort -n > /tmp/account_quota_exit
grep $domain /etc/userdomains | awk '{print $2}' > /tmp/account
diff -u /tmp/account_quota_exit /tmp/account | grep -E '^\+' | sed 's/^+//g' | sed '1d' > /tmp/account_quota_notfound

while IFS= read -r line
do
    echo -n "$line"
    touch /home/$line/etc/$domain/quota
    echo -n " ... "
    chown $line.mail /home/$line/etc/$domain/quota
    echo "Done"
done < /tmp/account_quota_notfound

# Check and create backup directory before backup config
## For iRedMail
x_backup_path_iredmail="/backup/iredmail"

if [ ! -d $x_backup_path_iredmail ]; then 
    mkdir -p $x_backup_path_iredmail
fi

## For Domain
x_backup_path_domain="/backup/iredmail/$domain"

if [ ! -d $x_backup_path_domain ]; then
    mkdir -p $x_backup_path_domain
fi

if [ ! -f $x_backup_path_domain/export-domain_suspend.csv ]; then
    touch $x_backup_path_domain/export-domain_suspend.csv
fi

# Report overview domain information
## Return domain name
DOMAIN=$domain
## Return account's package
PACKAGE=`grep -w $account /etc/userplans|awk -F ": " '{print $2}'`
## Return account's suspend
user_suspend=$(awk 'NR!=1' $x_backup_path_domain/export-domain_suspend.csv | awk -F ',' '{print $2}' | grep $account)
[ -n "$user_suspend" ] && STATUS="Disable" || STATUS="Active"
## Return account's quota size
QUOTA_SIZE=$(whmapi1 --output=jsonpretty accountsummary user=$account | jq -r '.data.acct[].disklimit')
## Return account's quota used
QUOTA_USED=$(whmapi1 --output=jsonpretty accountsummary user=$account | jq -r '.data.acct[].diskused')
## Return account's mailbox
MAILBOX=$(uapi --output=jsonpretty --user=$account Email list_pops no_validate=0 skip_main=1 | jq -r '.result.data[].email' | wc -l)
## Return account's forwarder
FORWARDER=$(uapi --user=$account Email count_forwarders|awk '$1 ~ /^data/ && !/autorespond/ {print $2}')
## Return account's filter
FILTER=$(uapi --user=$account Email count_filters|awk '$1 ~ /^data:/ {print $2}')
## Return account's autoresponders
AUTORESPONDERS=$(uapi --output=jsonpretty --user=$account Email count_auto_responders | jq -r '.result.data')
echo "$DOMAIN,$PACKAGE,$QUOTA_SIZE,$QUOTA_USED,$MAILBOX,$FORWARDER,$FILTER,$AUTORESPONDERS,$STATUS" >> $x_backup_path_domain/export-domain_overview.txt

###########   >_   #####################################################################
# Title for csv file when export domain information
echo domain_name,package_name,quota_size,max_users,max_aliases,max_lists > $x_backup_path_domain/export-domain_information.csv                                  

## Check directory etc exist for domain
if [ ! -d /home/$account/etc/$domain ] || [ ! -f /home/$account/etc/$domain/shadow ]
then
    continue
fi

## Export all domain information
domain_name=$(whmapi1 --output=jsonpretty accountsummary user=$account | jq -r '.data.acct[].domain')
package_name=$(whmapi1 --output=jsonpretty accountsummary user=$account | jq -r '.data.acct[].plan')
quota_size=$(whmapi1 --output=jsonpretty accountsummary user=$account | jq -r '.data.acct[].disklimit'| sed 's/M//g')
max_users=$(whmapi1 --output=jsonpretty accountsummary user=$account | jq -r '.data.acct[].maxpop')
max_aliases=$(whmapi1 --output=jsonpretty accountsummary user=$account | jq -r '.data.acct[].maxlst')
max_lists=$(whmapi1 --output=jsonpretty accountsummary user=$account | jq -r '.data.acct[].maxlst')

## Content for csv file
echo $domain_name,$package_name,$quota_size,$(expr $max_users + 1),$max_aliases,$max_lists >> $x_backup_path_domain/export-domain_information.csv

## Unsuspend email account incoming, outgoing and login
uapi --output=jsonpretty --user=$account Email list_pops no_validate=0 skip_main=1 | jq -r '.result.data[] | select(.suspended_login == 1) | .email' | while read mailbox_suspended
do
    uapi --output=jsonpretty --user=$account Email unsuspend_login email=$mailbox_suspended > /dev/null

    uapi --output=jsonpretty --user=$account Email unsuspend_incoming email=$mailbox_suspended > /dev/null

    uapi --output=jsonpretty --user=$account Email unsuspend_outgoing email=$mailbox_suspended > /dev/null

    echo $mailbox_suspended >> $x_backup_path_domain/export-mailboxes_suspended.$domain.csv
done

# ## Export mailboxes admin with password encode (MD5) of domain
awk -F ":" '{print $1, $2}' /etc/shadow | awk '$1 ~ /^'$account'/' | while read username_admin password_admin
do
    ### Convert MD5 password hash to CRYPT
    hash={CRYPT}$password_admin

    ### Combined mailboxes admin
    echo $domain,$username_admin,$hash,,, >> $x_backup_path_domain/export-mailboxes_information_admin.$domain.csv
done

## Export mailboxes with password encode (MD5) of domain
awk -F ":" '{print $1, $2}' /home/$account/etc/$domain/shadow | while read username password
do
    ### Convert MD5 password hash to CRYPT
    hash={CRYPT}$password

    ### Return email account's quota
    quota=$(uapi --output=jsonpretty --user=$account Email get_pop_quota as_bytes=1 email=$username | jq -r '.result.data')

    ### Return email account's forward
    forward=$(sed "s|:||g" /etc/valiases/$domain| awk '$1 ~ /^'$username@$domain'/ && !/autorespond/' | awk '{print $2}')

    if [ ! -n "$forward" ]
    then
        ### Combined mailboxes with domain, username, hash, quota and not forward
        echo $domain,$username,$hash,,$quota, >> $x_backup_path_domain/export-mailboxes_information_none_forward.$domain.csv
    else
        ### Combined mailboxes with domain, username, hash, quota and forward
        echo $domain,$username,$hash,,$quota,,$username@$domain,$forward >> $x_backup_path_domain/export-mailboxes_information_forward.$domain.csv
    fi

    ### Check directory mail exist for domain
    if [ ! -d /home/$account/mail ] || [ ! -d /home/$account/mail/$domain ]
    then
        continue
    fi

    ### Combine mailboxes with maildir
    echo $username@$domain:/home/$account/mail/$domain/$username/ >> $x_backup_path_domain/export-mailboxes_maildir.$domain
done

## Export mailing lists
uapi --output=jsonpretty --user=$account Email list_lists | jq -r '.result.data[].list' | while read x_list
do
    ### Convert @ -> _ for lists
    list=$(echo $x_list | sed 's/@/_/g')

    ### Return mailing list's subscribers
    member=$(/usr/local/cpanel/3rdparty/mailman/bin/list_members $list)

    if [ ! -n "$member" ]
    then
        continue
    fi
    ### Combine mailing lists with lists and member
    echo $member >> $x_backup_path_domain/export-list_$x_list
done

## Compress all files in directory backup
cd $x_backup_path_iredmail

tar czf $domain.tar.gz $domain

rm -rf $domain
}

# Main function, switches options passed to it
case "$1" in
    -h) helptext
    ;;
    --help) helptext

    case "$2" in
        --domain|-d) 
        domain "$3" "$4"
        ;;
    *)
        echo "Invalid Option!"
        helptext
        ;;
    esac
        ;;
        --domain|-d)
        if [ -z $2 ] || [ -z $3 ]; then
            echo "Invalid Option! Missing arguments for domain or server."
            helptext
            exit 1
        elif  [ $# -gt 3 ]; then
            echo "Too many arguments, should only add 1 domain and 1 server argument."
            helptext
            exit 1
        else
        domain "$2" "$3"
        fi
        ;;
    *)
        echo "Invalid Option!"
        helptext
        ;;                
esac