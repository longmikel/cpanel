#!/usr/bin/env python3
import csv
import requests

# URL
url = 'https://ired.mcloud.vn/iredadmin/api'

# Admin email address and password.
admin = 'api@email.emailserver.vn'
pw = 'j3ANxPd@ige29trFDIoZYiBXBO'

# Specify the path to the CSV file directly
x_path = '/home/ubuntu/iredmail/export-domain_information.csv'
    
# Define a class api login to iRedAdmin
class iRedAdminAPI:
    def __init__(self, base_url, admin_email, admin_password):
        self.base_url = base_url
        self.admin_email = admin_email
        self.admin_password = admin_password
        self.cookies = None

    def login(self):
        login_url = f'{self.base_url}/login'
        response = requests.post(login_url, data={'username': self.admin_email, 'password': self.admin_password})
        data = response.json()
        if data['_success']:
            self.cookies = response.cookies
        return data['_success']
    
    def create_domain(self, domain_name, package_data):
        if not self.cookies:
            return False
        
        create_domain_url = f'{self.base_url}/domain/{domain_name}'
        response = requests.post(create_domain_url, cookies=self.cookies, data=package_data)
        return response
        
    def logout(self):
        self.cookies = None

class PackageData:
    def __init__(self, name_plan, quota_size, max_users, max_aliases, max_lists):
        self.data = {
            'name': str(name_plan),
            'quota': str(quota_size),
            'numberOfUsers': str(max_users),
            'numberOfAliases': str(max_aliases),
            'numberOfLists': str(max_lists)
        }

# Create a list to store domain data
domain_data = []

# Read domain data from the CSV file
with open(x_path, 'r', newline='') as csvfile:
    reader = csv.DictReader(csvfile)
    for row in reader:
        domain_name = row['domain_name']
        package_name = row['package_name']
        quota_size = int(row['quota_size'])
        max_users = int(row['max_users'])
        max_aliases = int(row['max_aliases'])
        max_lists = int(row['max_lists'])
        
        package_data = PackageData(package_name, quota_size, max_users, max_aliases, max_lists).data
        domain_data.append((domain_name, package_data))

# Login
api = iRedAdminAPI(url, admin, pw)

if api.login():
    for domain_name, package_data in domain_data:
        api.create_domain(domain_name, package_data)
            
    # Perform other operations if needed
    
    api.logout()
else:
    print('Login failed')

if __name__ == '__main__':
    pass