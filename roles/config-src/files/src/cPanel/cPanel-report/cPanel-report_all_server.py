import subprocess
import os
import json
import socket
import ssl
import re
from datetime import datetime

# Set variables
x_path = "/backup/report"
x_server_hostname = socket.gethostname()

# Color variables
red = "\033[1;31m"
green = "\033[1;32m"
yellow = "\033[1;33m"
blue = "\033[1;34m"
reset = "\033[0m"

# Function to execute shell command and return output
def execute_shell_command(command):
    result = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    output, error = result.communicate()
    return output.decode().strip()

# Get current time
def get_current_time():
    return datetime.now().timestamp()

# Function to fix file quota if not exists
def fix_file_quota():
    accounts = execute_shell_command("cat /etc/userdomains | awk '{print $2}' | sort -n | sed '/nobody/d' | sed '/: salehost/d'")
    accounts_quota_exit = execute_shell_command("ls /home/*/etc/*/quota | cut -d'/' -f3 | sort -n")
    accounts_quota_notfound = set(accounts.splitlines()) - set(accounts_quota_exit.splitlines())
    for account in accounts_quota_notfound:
        domain = execute_shell_command("cat /etc/userdomains | grep {} | cut -d: -f1".format(account))
        execute_shell_command("touch /home/{}/etc/{}/quota".format(account, domain))
        execute_shell_command("chown {}.mail /home/{}/etc/{}/quota".format(account, account, domain))
        
# Function to delete report folder if exists
def delete_report():
    execute_shell_command("rm -rf /backup/report")

# Function to report to folder /backup
def report_to_backup():
    x_total = len(execute_shell_command("awk -F ':' '{print $1 $2}' /etc/domainusers | sed '/salehost/d'").splitlines())
    os.makedirs(x_path, exist_ok=True)
    with open("{}/export-domain_suspend.csv".format(x_path), "w") as file:
        file.write("TIME,USER,DOMAIN,REASON\n")

# Function to check suspend for domain before report
def check_suspend_before_report():
    suspended_accounts = json.loads(execute_shell_command("whmapi1 --output=jsonpretty listsuspended"))
    for account in suspended_accounts['data']['account']:
        time_suspend = account['time']
        user_suspend = account['user']
        domain = execute_shell_command("cat /etc/userdomains | grep {} | awk '{{print $2}}'".format(user_suspend))
        reason_suspend = account['reason']
        with open("{}/export-domain_suspend.csv".format(x_path), "a") as file:
            file.write("{},{},{},{}\n".format(time_suspend, user_suspend, domain, reason_suspend))
    return {account['user'] for account in suspended_accounts['data']['account']}  # Return a set of suspended users

# Function to report server overview information
def report_server_information():
    x_server_hostname = execute_shell_command("hostname")
    x_server_ip = execute_shell_command("curl ipinfo.io/ip")
    x_server_cpu = execute_shell_command("nproc")
    x_server_ram = execute_shell_command("free -g | grep 'Mem:' | awk '{print$2}'")
    x_server_limit = int(execute_shell_command("df -BM /home | awk '{print$2}' | sed -n 2p").split("M")[0])
    x_server_used = int(execute_shell_command("df -BM /home | awk '{print$3}' | sed -n 2p").split("M")[0])
    x_server_free = int(execute_shell_command("df -BM /home | awk '{print$4}' | sed -n 2p").split("M")[0])
    x_server_domain = len(execute_shell_command("find /var/cpanel/users -type f ! -iname 'salehost' -print").splitlines())
    x_server_suspend = len(execute_shell_command("find /var/cpanel/suspended/ -type f -print").splitlines())
    x_server_active = x_server_domain - x_server_suspend
    x_server_mailbox = len(execute_shell_command("sed 's|:||g' /etc/userdomains | awk '!/^\* nobody/{print $1, $2 | \"sort | uniq\" }' | awk '{print \"cat /home/\"$2\"/etc/\"$1\"/passwd\"}' | sh 2>/dev/null").splitlines())
    server_info = {
        "IP": x_server_ip,
        "CPU": x_server_cpu,
        "RAM": x_server_ram,
        "Disk Limit": x_server_limit * 1024 * 1024,
        "Disk Usage": x_server_used * 1024 * 1024,
        "Disk Free": x_server_free * 1024 * 1024,
        "Domain Total": x_server_domain,
        "Domain Active": x_server_active,
        "Domain Suspend": x_server_suspend,
        "Mailbox Total": x_server_mailbox
    }
    with open("{}/export-{}_general.txt".format(x_path, x_server_hostname), "a") as file:
        file.write(json.dumps(server_info) + "\n")
        
# Function to return account's package
def return_account_package(user):
    return json.loads(execute_shell_command("whmapi1 --output=jsonpretty accountsummary user={}".format(user)))['data']['acct'][0]['plan']

# Function to return account's suspend status
def return_account_suspend(user):
    x_user_suspend = execute_shell_command("awk 'NR!=1' {}/export-domain_suspend.csv | awk -F ',' '{{print $2}}' | grep {}".format(x_path, user))
    return "Suspended" if x_user_suspend else "Actived"

# Function to return account's quota summary
def return_account_quota_summary(user):
    account_summary = json.loads(execute_shell_command("whmapi1 --output=jsonpretty accountsummary user={}".format(user)))['data']['acct'][0]
    x_limit = account_summary['disklimit'].split("M")[0]
    x_used = account_summary['diskused'].split("M")[0]
    
    if x_limit == 'unlimited':
        x_limit = 1
    else:
        x_limit = int(x_limit)
    if x_used == 'unlimited':
        x_used = 1
    else:
        x_used = int(x_used)
        x_percent = int(x_used / x_limit * 100)

    return {
        "Quota Limit": x_limit * 1024 * 1024,
        "Quota Usage": x_used * 1024 * 1024,
        "Quota Percent": x_percent
    }

# Function to return account's mailbox
def return_account_mailbox(user):
    x_mailbox_limit = json.loads(execute_shell_command("whmapi1 --output=jsonpretty accountsummary user={}".format(user)))['data']['acct'][0]['maxpop']
    x_mailbox = len(execute_shell_command("uapi --output=jsonpretty --user={} Email list_pops no_validate=0 skip_main=1 | jq -r '.result.data[].email'".format(user)).splitlines())
    
    if x_mailbox_limit == 'unlimited':
        x_mailbox_limit = 1
    else:
        x_mailbox_limit = int(x_mailbox_limit)
        
    return {
        "Mailbox Limit": x_mailbox_limit,
        "Mailbox Usage": x_mailbox
    }

# Function to return account validate SPF records
def return_account_spf(domain):
    return json.loads(execute_shell_command("whmapi1 --output=jsonpretty validate_current_spfs domain='{}'".format(domain)))['data']['payload'][0]['state']

# Function to return account validate DKIM records
def return_account_dkim(domain):
    return json.loads(execute_shell_command("whmapi1 --output=jsonpretty validate_current_dkims domain='{}'".format(domain)))['data']['payload'][0]['state']

# Function to check SSL certificate for a domain
def verify_ssl_certificate(domain):
    try:
        hostname = "mail.{}".format(domain)
        with socket.create_connection((hostname, 443), timeout=10) as sock:
            with ssl.create_default_context().wrap_socket(sock, server_hostname=hostname) as ssock:
                cert = ssock.getpeercert()
                return cert
    except Exception as e:
        data = {
            "Issue": "{}".format(e),
            "Reason": "Invalid Certificate"
        }
        return data

# Main function
def main():
    res1 = get_current_time()
    delete_report()
    report_to_backup()
    
    # Get list of suspended users to skip
    suspended_users = check_suspend_before_report()
    
    report_server_information()

    with open("/etc/trueuserdomains", "r") as file:
        for line in file:
            domain, user = line.strip().split(": ")
            
            # Skip suspended accounts
            if user in suspended_users:
                continue

            x_package = return_account_package(user)
            x_status = return_account_suspend(user)
            quota_summary = return_account_quota_summary(user)
            mailbox_info = return_account_mailbox(user)
            x_spf = return_account_spf(domain)
            x_dkim = return_account_dkim(domain)
            certificate_info = verify_ssl_certificate(domain)
            account_info = {
                "Domain": domain,
                "Package": x_package,
                **quota_summary,
                **mailbox_info,
                "SPF": x_spf,
                "DKIM": x_dkim,
                **certificate_info,
                "Status": x_status
            }
            with open("{}/export-{}_general.txt".format(x_path, x_server_hostname), "a") as file:
                file.write(json.dumps(account_info) + "\n")
                
if __name__ == "__main__":
    main()