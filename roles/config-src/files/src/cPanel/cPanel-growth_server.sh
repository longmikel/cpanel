#!/bin/bash
SV=$(cat /etc/zabbix/scripts/server.txt)
DOMAIN_TOTAL=$(find /var/cpanel/users ! -name "system" -type f -print | wc -l)
DOMAIN_SUSPENDED=$(find /var/cpanel/suspended/ -type f -print | wc -l)
DOMAIN_ACTIVE=$(expr $DOMAIN_TOTAL - $DOMAIN_SUSPENDED)
MAILBOX=$(sed "s|:||g" /etc/userdomains | awk '!/^\* nobody/{print $1, $2 | "sort | uniq" }' | awk '{print "cat /home/"$2"/etc/"$1"/passwd"}' | sh 2>/dev/null | wc -l)

# Create JSON string
echo '{''"SV"':$SV',''"DOMAIN_TOTAL"':$DOMAIN_TOTAL',''"DOMAIN_SUSPENDED"':$DOMAIN_SUSPENDED',''"DOMAIN_ACTIVE"':$DOMAIN_ACTIVE',''"MAILBOX"':$MAILBOX'}' >> /var/log/email.growth
