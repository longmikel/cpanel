#!/usr/bin/env python3
# encoding: utf-8

# Author: Zhang Huangbin <zhb _at_ iredmail.org>
# Purpose: Add new OpenLDAP user for postfix mail server.
# Project:  iRedMail (http://www.iredmail.org/)

# --------------------------- WARNING ------------------------------
# This script only works under iRedMail >= 0.4.0 due to ldap schema
# changes.
# ------------------------------------------------------------------

# ---------------------------- USAGE -------------------------------
# Put your user list in a csv format file, e.g. users.csv, and then
# import users listed in the file:
#
#   $ python3 create_mail_user_OpenLDAP_alias.py alias.csv
#
# ------------------------------------------------------------------

# ------------------------- SETTINGS -------------------------------
# LDAP server address.
LDAP_URI = 'ldap://127.0.0.1:389'

# LDAP base dn.
BASEDN = 'o=domains,dc=emailserver,dc=vn'

# Bind dn/password
BINDDN = 'cn=Manager,dc=emailserver,dc=vn'
BINDPW = '4maYJNQGvpMyU4uxIU4HaMvi7K7sPgMH'

# Storage base directory.
STORAGE_BASE_DIRECTORY = '/var/vmail/vmail1'

# Append timestamp in maildir path.
APPEND_TIMESTAMP_IN_MAILDIR = True

# Get base directory and storage node.
std = STORAGE_BASE_DIRECTORY.rstrip('/').split('/')
STORAGE_NODE = std.pop()
STORAGE_BASE = '/'.join(std)

# Hashed maildir: True, False.
# Example:
#   domain: domain.ltd,
#   user:   zhang (zhang@domain.ltd)
#
#       - hashed: d/do/domain.ltd/z/zh/zha/zhang/
#       - normal: domain.ltd/zhang/
HASHED_MAILDIR = True

# Default password schemes.
# Multiple passwords are supported if you separate schemes with '+'.
# For example: 'SSHA+NTLM', 'CRAM-MD5+SSHA', 'CRAM-MD5+SSHA+MD5'.
DEFAULT_PASSWORD_SCHEME = 'SSHA512'

# Do not prefix password scheme name in password hash.
HASHES_WITHOUT_PREFIXED_PASSWORD_SCHEME = ['NTLM']
# ------------------------------------------------------------------

import os
import sys
import time
import datetime
from subprocess import Popen, PIPE
from typing import List, Dict
import re

try:
    import ldif
    import ldap
except ImportError:
    print("""
    Error: You don't have python-ldap installed, Please install it first.

    You can install it like this:

    - On RHEL/CentOS 5.x:

        $ sudo yum install python-ldap

    - On Debian & Ubuntu:

        $ sudo apt install python-ldap
    """)
    sys.exit()

def usage():
    print("""
CSV file format:

    domain name, alias, [common name], forwarding

Example #1:
    preview-pro.mcloud.vn, zhang, Zhang Huangbin, zang@preview-promcloudvn.onmicrosoft.com

Note:
    - Domain name, alias and password are REQUIRE, others are optional:
        + common name.
            * It will be the same as username if it's empty.
            * Non-ascii character is allowed in this field, they will be
              encoded automaticly. Such as Chinese, Korea, Japanese, etc.
        + forwarding.
            * valid name (hr@a.cn): hr
            * incorrect group name: hr@a.cn
            * Do *NOT* include domain name in group name, it will be
              appended automaticly.
            * Multiple forwarding must be seperated by colon.
    - Leading and trailing Space will be ignored.
    """)

def __str2bytes(s) -> bytes:
    """Convert `s` from string to bytes."""
    if isinstance(s, bytes):
        return s
    elif isinstance(s, str):
        return s.encode()
    elif isinstance(s, (int, float)):
        return str(s).encode()
    else:
        return bytes(s)

def __attr_ldif(attr, value, default=None) -> List:
    """Generate a list of LDIF data with given attribute name and value.
    Returns empty list if no valid value.

    Value is properly handled with str/bytes/list/tuple/set types, and
    converted to list of bytes at the end.

    To generate ldif list with ldap modification like `ldap.MOD_REPLACE`,
    please use function `mod_replace()` instead.
    """
    _ldif = []
    v = value or default
    if v:
        if isinstance(value, (list, tuple, set)):
            lst = []
            for i in v:
                # Avoid duplicate element.
                if i in lst:
                    continue

                if isinstance(i, bytes):
                    lst.append(i)
                else:
                    lst.append(__str2bytes(i))

            _ldif = [(attr, lst)]
        else:
            _ldif = [(attr, [__str2bytes(v)])]

    return _ldif


def __attrs_ldif(kvs: Dict) -> List:
    lst = []
    for (k, v) in kvs.items():
        lst += __attr_ldif(k, v)

    return lst

# Return list of modification operation.
def mail_to_user_dn(mail):
    """Convert email address to ldap dn of normail mail user."""
    if mail.count('@') != 1:
        return ''

    user, domain = mail.split('@')

    # User DN format.
    # mail=myalias@domain.ltd,domainName=domain.ltd,[LDAP_BASEDN]
    dn = 'mail=%s,ou=Aliases,domainName=%s,%s' % (mail, domain, BASEDN)

    return dn

def get_days_of_today():
    """Return number of days since 1970-01-01."""
    today = datetime.date.today()

    try:
        return (datetime.date(today.year, today.month, today.day) - datetime.date(1970, 1, 1)).days
    except:
        return 0

def ldif_mailuser(domain, username, cn, *forwarding_address):
    # Append timestamp in maildir path
    DATE = time.strftime('%Y.%m.%d.%H.%M.%S')
    TIMESTAMP_IN_MAILDIR = ''
    if APPEND_TIMESTAMP_IN_MAILDIR:
        TIMESTAMP_IN_MAILDIR = '-%s' % DATE

    # Remove SPACE in username.
    username = str(username).lower().strip().replace(' ', '')

    if cn == '':
        cn = username

    mail = username + '@' + domain
    dn = mail_to_user_dn(mail)
    
    # Check if the mailbox already exists
    conn = ldap.initialize(LDAP_URI)
    conn.set_option(ldap.OPT_PROTOCOL_VERSION, 3)
    conn.simple_bind_s(BINDDN, BINDPW)

    # Search for the user entry
    try:
        result = conn.search_s(BASEDN, ldap.SCOPE_SUBTREE, "(mail=%s)" % mail, ['mail'])
    except ldap.LDAPError:
        result = None

    conn.unbind()

    if result:
        print(f"Skipping existing mailbox: {mail}")
        return None, None

    _ldif = __attrs_ldif({
        'objectClass': ['mailAlias'],
        'mail': mail,
        'cn': cn,
        'accountStatus': 'active',
        'enabledService': ['mail',
                           'deliver',
                           'displayedInGlobalAddressBook'],
        'mailForwardingAddress': forwarding_address,
    })

    return dn, _ldif

if len(sys.argv) != 2 or len(sys.argv) > 2:
    print("""Usage: $ python3 %s users.csv""" % sys.argv[0])
    usage()
    sys.exit()
else:
    CSV = sys.argv[1]
    if not os.path.exists(CSV):
        print("Error: file not exist:", CSV)
        sys.exit()

ldif_file = CSV + '.ldif'

# Remove exist LDIF file.
if os.path.exists(ldif_file):
    print("< INFO > Remove exist file:", ldif_file)
    os.remove(ldif_file)

# Read user list.
userList = open(CSV, 'rb')

# Convert to LDIF format.
for entry in userList.readlines():
    entry = entry.decode().rstrip()
    domain, username, cn, *forwarding_address = re.split(r'\s?,\s?', entry)
    dn, data = ldif_mailuser(domain, username, cn, *forwarding_address)

    if dn and data:
        # Write LDIF data.
        result = open(ldif_file, 'a')
        ldif_writer = ldif.LDIFWriter(result)
        ldif_writer.unparse(dn, data)

ldif_file_path = os.path.abspath(ldif_file)
print("< INFO > User data are stored in %s, you can verify it before importing it." % ldif_file_path)
print("< INFO > You can import it with below command:")
print("ldapadd -x -D %s -W -f %s" % (BINDDN, ldif_file_path))

# Prompt to import user data.
"""
answer = raw_input("Would you like to import them now? [y|N]").lower().strip()

if answer == 'y':
    # Import data.
    conn = ldap.initialize(LDAP_URI)
    conn.set_option(ldap.OPT_PROTOCOL_VERSION, 3)   # Use LDAP v3
    conn.bind_s(BINDDN, BINDPW)
    conn.unbind()
else:
    pass
"""