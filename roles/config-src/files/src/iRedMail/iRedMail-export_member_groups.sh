#!/bin/bash
# Use domain
if [ -z "$1" ]; then
    echo Usage: $0 {domain}
    exit 1
fi

# Define variables
x_tools="/usr/local/src/iRedMail"
x_binddn="cn=Manager,dc=emailserver,dc=vn"
x_basedn="o=domains,dc=emailserver,dc=vn"
x_bindpw=`cat $x_tools/vault/secret-ldap.txt | openssl enc -aes-256-cbc -md sha512 -a -d -pbkdf2 -iter 100000 \
 -salt -pass pass:Secret@123#`

# Define html
echo "<!DOCTYPE html>
<html lang="vi">
<head>
    <meta charset="UTF-8">
    <title>Hiển thị thông tin màu sắc</title>
    <style>
        /* Định dạng chung */
        body {
            font-family: Arial, sans-serif;
        }

        /* Định dạng cho thông báo */
        .check {
            color: purple; /* Màu tím cho phần kiểm tra */
            font-weight: bold; /* Tăng độ đậm cho phần CHECK */
        }
        .info {
            color: blue; /* Màu xanh dương cho phần thông tin */
            font-weight: bold; /* Tăng độ đậm cho phần INFO */
        }
        .success {
            color: green; /* Màu xanh lá cho phần hoàn thành */
            font-weight: bold; /* Tăng độ đậm cho phần SUCCESS */
        }
        .error {
            color: red; /* Màu đỏ cho phần lỗi */
            font-weight: bold; /* Tăng độ đậm cho phần ERROR */
        }
    </style>
</head>
</html>
"

# Check domain exist
echo "<!DOCTYPE html>
<html lang="vi">
    <body>
        <p><span class="check">[CHECK]</span> - Preparing to check domain $1, please wait...</p>
    </body>
</html>
"
sleep 3
i=$(ldapsearch -x -o ldif-wrap=no -D $x_binddn -w $x_bindpw -b $x_basedn domainName | awk '/domainName:/ {print $2}' | awk "/$1/")
if [ -z $i ]; then
    echo "<!DOCTYPE html>
    <html lang="vi">
        <body>
            <p><span class="error">[ERROR]</span> - Domain $1 does not exists on the server</p>
        </body>
    </html>
    "
    exit 1
else
    echo "<!DOCTYPE html>
    <html lang="vi">
        <body>
            <p><span class="info">[INFO]</span> - Domain $1 exists on the server</p>
        </body>
    </html>
    "
fi

domain=$1

x_groups=$(ldapsearch -x -o ldif-wrap=no -D $x_binddn -w $x_bindpw -b "ou=Groups,domainName=$domain,o=domains,dc=emailserver,dc=vn" mail | awk '/mail=/' | cut -d ',' -f1 |cut -d ' ' -f2|sed 's/mail=//g')

# Build HTML table header
html_output="
<style>
    table {
      width: 50%; /* Adjust as needed for table width */
      border-collapse: collapse; /* Remove default table borders */
    }

    th, td {
        border: 1px solid #ddd; /* Add a thin border for each cell */
        padding: 8px; /* Add some padding for spacing */
        text-align: center; /* Center text within cells */
    }
</style>
<table>
    <tr>
        <th>Email Groups</th>
        <th>Member for Groups</th>
    </tr>
"

# Get member for list
echo "<!DOCTYPE html>
<html lang="vi">
    <body>
        <p><span class="info">[INFO]</span> - Preparing to get member in list of domain $1, please wait...</p>
    </body>
</html>
"
sleep 3

while read -r x_groups; do
    x_members=$(python3 /opt/mlmmjadmin/tools/maillist_admin.py subscribers $x_groups | cut -d ',' -f1)

    # Escape special characters for group name
    escaped_x_groups=$(printf "%s" "$x_groups" | sed 's/&/&amp;/g; s/</&lt;/g; s/>/&gt;/g')

    # Initialize empty member list string
    escaped_members=""

    # Iterate over members and append escaped names
    for member in $x_members; do
        escaped_member=$(printf "%s" "$member" | sed 's/&/&amp;/g; s/</&lt;/g; s/>/&gt;/g')
        escaped_members+="$escaped_member<br/>" # Add a line break after each member
    done

    # Remove trailing line break
    escaped_members=${escaped_members%<br/>}

    # Add table row with escaped group and members
    html_output+="
    <tr>
        <td>$escaped_x_groups</td>
        <td>$escaped_members</td>
    </tr>"
done <<< "$x_groups"

# Close HTML table and print output
html_output+="</table>"
echo -e "$html_output"

exit 0
