#!/bin/bash
# Use domain
if [ -z "$1" ]; then
    echo Usage: $0 {domain}
    exit 1
fi

# Define variables
x_binddn="cn=Manager,dc=emailserver,dc=vn"

x_basedn="o=domains,dc=emailserver,dc=vn"

x_bindpw="4maYJNQGvpMyU4uxIU4HaMvi7K7sPgMH"

# Define html
echo "<!DOCTYPE html>
<html lang="vi">
<head>
    <meta charset="UTF-8">
    <title>Hiển thị thông tin màu sắc</title>
    <style>
        /* Định dạng chung */
        body {
            font-family: Arial, sans-serif;
        }

        /* Định dạng cho thông báo */
        .check {
            color: purple; /* Màu tím cho phần kiểm tra */
            font-weight: bold; /* Tăng độ đậm cho phần CHECK */
        }
        .info {
            color: blue; /* Màu xanh dương cho phần thông tin */
            font-weight: bold; /* Tăng độ đậm cho phần INFO */
        }
        .success {
            color: green; /* Màu xanh lá cho phần hoàn thành */
            font-weight: bold; /* Tăng độ đậm cho phần SUCCESS */
        }
        .error {
            color: red; /* Màu đỏ cho phần lỗi */
            font-weight: bold; /* Tăng độ đậm cho phần ERROR */
        }
    </style>
</head>
</html>
"

# Check domain exist
echo "<!DOCTYPE html>
<html lang="vi">
    <body>
        <p><span class="check">[CHECK]</span> - Preparing to check domain $1, please wait...</p>
    </body>
</html>
"
sleep 3
i=$(ldapsearch -x -o ldif-wrap=no -D $x_binddn -w $x_bindpw -b $x_basedn domainName | awk '/domainName:/ {print $2}' | awk "/$1/")
if [ -z $i ]; then
    echo "<!DOCTYPE html>
    <html lang="vi">
        <body>
            <p><span class="error">[ERROR]</span> - Domain $1 does not exists on the server</p>
        </body>
    </html>
    "
    exit 1
else
    echo "<!DOCTYPE html>
    <html lang="vi">
        <body>
            <p><span class="info">[INFO]</span> - Domain $1 exists on the server</p>
        </body>
    </html>
    "
fi

# Build HTML table header
html_output="
<style>
    table {
      width: 50%; /* Adjust as needed for table width */
      border-collapse: collapse; /* Remove default table borders */
    }

    th, td {
        border: 1px solid #ddd; /* Add a thin border for each cell */
        padding: 8px; /* Add some padding for spacing */
        text-align: center; /* Center text within cells */
    }
</style>
<table>
    <tr>
        <th>Email Address</th>
        <th>Forward mails to address</th>
    </tr>
"

# Get member for list
echo "<!DOCTYPE html>
<html lang="vi">
    <body>
        <p><span class="info">[INFO]</span> - Prepare to export the mailbox's forwarding list of domain $1, please wait...</p>
    </body>
</html>
"
sleep 3

domain=$1

check_fw () {
ldapsearch -x -o ldif-wrap=no -D $x_binddn -w $x_bindpw -b "ou=Users,domainName=$domain,o=domains,dc=emailserver,dc=vn" mail | awk '/mail=/' | cut -d ',' -f1 |cut -d ' ' -f2|sed 's/mail=//g' | while read x_mailbox
do
        ldapsearch -x -o ldif-wrap=no -D $x_binddn -w $x_bindpw -b "mail=$x_mailbox,ou=Users,domainName=$domain,o=domains,dc=emailserver,dc=vn" mailForwardingAddress | awk '/mailForwardingAddress:/ {print $2}' | while read x_forwarder
    do
        email_data="$x_mailbox"
        echo "$email_data" >> $domain
    done
done
}

# Close HTML table and print output
:> $domain
check_fw

while IFS= read -r x_users; do
        x_forwarders=$(ldapsearch -x -o ldif-wrap=no -D $x_binddn -w $x_bindpw -b "mail=$x_users,ou=Users,domainName=$domain,o=domains,dc=emailserver,dc=vn" mailForwardingAddress | awk '/mailForwardingAddress:/ {print $2}')
        escaped_x_users=$(printf "%s" "$x_users" | sed 's/&/&amp;/g; s/</&lt;/g; s/>/&gt;/g')
        escaped_x_forwarder=$(printf "%s" "$x_forwarders" | sed 's/&/&amp;/g; s/</&lt;/g; s/>/&gt;/g')
        # Add table row with escaped data
        html_output+="
        <tr>
                <td>$escaped_x_users</td>
                <td>$escaped_x_forwarder</td>
        </tr>"
done < $domain
html_output+="</table>"
echo -e "$html_output"

exit 0