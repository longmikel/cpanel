#!/bin/bash
# Use domain
if [ -z "$1" ]; then
    echo Usage: $0 {mailbox}
    exit 1
fi

# Define variables
x_tools="/usr/local/src/iRedMail"
x_binddn="cn=Manager,dc=emailserver,dc=vn"
x_basedn="o=domains,dc=emailserver,dc=vn"
x_bindpw=`cat $x_tools/vault/secret-ldap.txt | openssl enc -aes-256-cbc -md sha512 -a -d -pbkdf2 -iter 100000 \
 -salt -pass pass:Secret@123#`

# Define html
echo "<!DOCTYPE html>
<html lang="vi">
<head>
    <meta charset="UTF-8">
    <title>Hiển thị thông tin màu sắc</title>
    <style>
        /* Định dạng chung */
        body {
            font-family: Arial, sans-serif;
        }

        /* Định dạng cho thông báo */
        .check {
            color: purple; /* Màu tím cho phần kiểm tra */
            font-weight: bold; /* Tăng độ đậm cho phần CHECK */
        }
        .info {
            color: blue; /* Màu xanh dương cho phần thông tin */
            font-weight: bold; /* Tăng độ đậm cho phần INFO */
        }
        .success {
            color: green; /* Màu xanh lá cho phần hoàn thành */
            font-weight: bold; /* Tăng độ đậm cho phần SUCCESS */
        }
        .error {
            color: red; /* Màu đỏ cho phần lỗi */
            font-weight: bold; /* Tăng độ đậm cho phần ERROR */
        }
    </style>
</head>
</html>
"

# Check domain exist
echo "<!DOCTYPE html>
<html lang="vi">
    <body>
        <p><span class="check">[CHECK]</span> - Preparing to check mailbox $1, please wait...</p>
    </body>
</html>
"
sleep 3
i=$(ldapsearch -x -o ldif-wrap=no -D $x_binddn -w $x_bindpw -b $x_basedn "(mail=$mailbox)" mail | awk '/mail:/ {print $2}')
if [ -z $i ]; then
    echo "<!DOCTYPE html>
    <html lang="vi">
        <body>
            <p><span class="error">[ERROR]</span> - Mailbox $1 does not exists on the server</p>
        </body>
    </html>
    "
    exit 1
else
    echo "<!DOCTYPE html>
    <html lang="vi">
        <body>
            <p><span class="info">[INFO]</span> - Mailbox $1 exists on the server</p>
        </body>
    </html>
    "
fi

# Change existing dovecot maildir installation from UTF-8 charset to m-UTF-7
mailbox=$(echo $1 | tr '[:upper:]' '[:lower:]')
x_maildir=$(ldapsearch -x -o ldif-wrap=no -D $x_binddn -w $x_bindpw -b $x_basedn "(mail=$mailbox)" homeDirectory | awk '/homeDirectory:/ {print $2}')
if [ -n $x_maildir ]; then
    # Check maildir for mailbox
    echo "<!DOCTYPE html>
    <html lang="vi">
        <body>
            <p><span class="info">[INFO]</span> - Changing exist maildir from UTF-8 charset to m-UTF-7 of $mailbox, please wait...</p>
        </body>
    </html>
    "
    # Into directory
    sleep 3
    cd ${x_maildir}Maildir

    doveadm mailbox list -7 -A | grep $mailbox | sort -r | while read user folder; do
        utf7=$(doveadm mailbox mutf7 -8 "$folder")
        utf8=$(doveadm mailbox mutf7 -7 "$folder")

        if [ "$utf7" != "$utf8" ]; then
            utf8_dir=".$(echo "$utf8" | tr "/" ".")"
            utf7_dir=$(doveadm mailbox mutf7 -8 "$utf8_dir")
            ls -ah -Q . | grep "$utf8_dir" | while read dir; do

                utf8_temp_dir=$(echo "$dir" | sed 's/^"//; s/"$//')
                utf7_temp_dir=$(doveadm mailbox mutf7 -8 "$utf8_temp_dir")
                
                if [ -e "$utf8_temp_dir" ]; then
                    mv -vT "$utf8_temp_dir" "$utf7_temp_dir"
                fi
            done
        fi
    done
    echo "<!DOCTYPE html>
    <html lang="vi">
        <body>
            <p><span class="success">[SUCCESS]</span> - Maildir from UTF-8 charset to m-UTF-7 of $mailbox changed complete</p>
        </body>
    </html>
    "
else
    echo "<!DOCTYPE html>
    <html lang="vi">
        <body>
            <p><span class="error">[ERROR]</span> - The maildir file of $mailbox was not found, please check again...</p>
        </body>
    </html>
    "
    exit 1
fi
exit 0