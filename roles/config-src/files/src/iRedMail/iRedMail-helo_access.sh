#!/bin/sh
# Use Domain
if [ -z "$1" ]; then
    echo Usage: $0 {partner} {domain}
    exit 1
fi

# Define variables
x_tools="/usr/local/src/iRedMail"
x_binddn="cn=Manager,dc=emailserver,dc=vn"
x_basedn="o=domains,dc=emailserver,dc=vn"
x_bindpw=`cat $x_tools/vault/secret-ldap.txt | openssl enc -aes-256-cbc -md sha512 -a -d -pbkdf2 -iter 100000 \
 -salt -pass pass:Secret@123#`

# Define html
echo "<!DOCTYPE html>
<html lang="vi">
<head>
    <meta charset="UTF-8">
    <title>Hiển thị thông tin màu sắc</title>
    <style>
        /* Định dạng chung */
        body {
            font-family: Arial, sans-serif;
        }

        /* Định dạng cho thông báo */
        .check {
            color: purple; /* Màu tím cho phần kiểm tra */
            font-weight: bold; /* Tăng độ đậm cho phần CHECK */
        }
        .info {
            color: blue; /* Màu xanh dương cho phần thông tin */
            font-weight: bold; /* Tăng độ đậm cho phần INFO */
        }
        .success {
            color: green; /* Màu xanh lá cho phần hoàn thành */
            font-weight: bold; /* Tăng độ đậm cho phần SUCCESS */
        }
        .error {
            color: red; /* Màu đỏ cho phần lỗi */
            font-weight: bold; /* Tăng độ đậm cho phần ERROR */
        }
    </style>
</head>
</html>
"

# Check domain exist
echo "<!DOCTYPE html>
<html lang="vi">
    <body>
        <p><span class="check">[CHECK]</span> - Preparing to check domain $2, please wait...</p>
    </body>
</html>
"
sleep 3
i=$(ldapsearch -x -o ldif-wrap=no -D $x_binddn -w $x_bindpw -b $x_basedn domainName | awk '/domainName:/ {print $2}' | awk "/$2/")
if [ -z $i ]; then
    echo "<!DOCTYPE html>
    <html lang="vi">
        <body>
            <p><span class="error">[ERROR]</span> - Domain $2 does not exists on the server</p>
        </body>
    </html>
    "
    exit 1
else
    continue
fi

# File config helo_access.pcre
config_file="/etc/postfix/helo_access.pcre"

partner=$1
domain=$2

# Change domain to regex
partner_to_regex=$(echo "/(.*$partner)$/")

if grep -q "/(.*$partner)$/" "$config_file"; then
    echo "<!DOCTYPE html>
    <html lang="vi">
        <body>
            <p><span class="info">[INFO]</span> - Partner $partner of domain $domain exists in config helo_access.pcre</p>
        </body>
    </html>
    "
else

# Find first line for partner
line_number=$(sed -n "/^# Partner of $domain$/=" "$config_file")

# If the group is not found, add a new group at the end of the file
if [ -n "$line_number" ]; then
    # If the previous line is not a #, add a blank line
    if ! grep -q "^#" "$config_file" | tail -n 1; then
        echo >> "$config_file"
    fi
        # If the group is found, add the content in the found group
        sed -i "$line_number a\/(.*$partner)$/ OK" "$config_file"
        echo "<!DOCTYPE html>
        <html lang="vi">
            <body>
                <p><span class="success">[SUCCESS]</span> - Added partner $partner of domain $domain in config helo_access.pcre</p>
            </body>
        </html>
        "
        postfix reload
else
    # If the group is not found, add a new group at the end of the file.
    echo >> "$config_file"  # Add blank line before creating new group
    echo "# Partner of $domain" >> "$config_file"
    echo "/(.*$partner)$/ OK" >> "$config_file"
    echo "<!DOCTYPE html>
    <html lang="vi">
        <body>
            <p><span class="success">[SUCCESS]</span> - Created new group $domain and added partner $partner of domain $domain in config helo_access.pcre</p>
        </body>
    </html>
    "
    postfix reload
    fi
fi

while [ $# -gt 1 ]; do
    key=$domain_to_regex
    value="$2"
    domain="$3"
    new_line="$new_line$key $value\n"
    shift 2
done