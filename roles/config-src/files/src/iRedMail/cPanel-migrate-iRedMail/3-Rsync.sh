#!/bin/bash

red=`tput setaf 1`
green=`tput setaf 2`
yellow=`tput setaf 3`
blue=`tput setaf 4`
reset=`tput sgr0`
res1=$(date +%s.%N)
me=`basename "$0"`
count=1
###########   >_   #####################################################################

# Set verbose to null
verbose=""

# Print the help text
helptext () {
    tput bold
    tput setaf 2
    echo "[iRedMail]"
    tput bold
    tput setaf 3
    echo "USAGE: $0 [Options] [Server] or [Options] [Domain] [Server]"
    tput bold
    tput setaf 4
    echo "Options:"
    echo "  -h, --help     display this help and exit"
    echo "  -a, --all      run rsync for all domain from source server old"
    echo "  -d, --domain   select one domain will rsync data from source server old"
    echo "."
    echo "."
    echo "."
        tput bold
        tput setaf 1
        echo "- Only run 1 time!"
    tput sgr0
    exit 0
}

helptext_all () {
    tput bold
    tput setaf 2
    echo "[iRedMail]"
    tput bold
    tput setaf 3
    echo "USAGE: $0 [Options] [Server]"
    tput bold
    tput setaf 4
    echo "Options:"
    echo "  -h, --help     display this help and exit"
    echo "  -a, --all      run rsync for all domain from source server old"
    echo "."
    echo "."
    echo "."
        tput bold
        tput setaf 1
        echo "- Only run 1 time!"
    tput sgr0
    exit 0
}

helptext_domain () {
    tput bold
    tput setaf 2
    echo "[iRedMail]"
    tput bold
    tput setaf 3
    echo "USAGE: $0 [Options] [Domain] [Server]"
    tput bold
    tput setaf 4
    echo "Options:"
    echo "  -h, --help     display this help and exit"
    echo "  -d, --domain   select one domain will rsync data from source server old"
    echo "."
    echo "."
    echo "."
        tput bold
        tput setaf 1
        echo "- Only run 1 time!"
    tput sgr0
    exit 0
}

all () {
###########   >_   #####################################################################
# Rsync
server=$1
cd /home/ubuntu/iredmail
cat /dev/null > list.txt
ls *.tar.gz > list.txt
x_total=$(cat list.txt | wc -l)

echo "[ ${blue}INFO${reset} ] Running Rsync Data for All Domains"
cat list.txt | sed 's/.tar.gz//g' | while read domain
do
    echo -n "Rsync - $domain "
    echo -n "- ($count/$x_total) "
    count=$(($count+1))

    # Define variables
    x_path="/home/ubuntu/iredmail/$domain"

    x_binddn="cn=Manager,dc=emailserver,dc=vn"

    x_basedn="o=domains,dc=emailserver,dc=vn"
        
    x_bindpw="4maYJNQGvpMyU4uxIU4HaMvi7K7sPgMH"
    
    # Manage maildir
    awk -F ":" '{print $1, $2}' $x_path/export-mailboxes_maildir.$domain | while read mailbox source_maildir
    do
        target_maildir=$(ldapsearch -x -o ldif-wrap=no -D $x_binddn -w $x_bindpw -b $x_basedn "(mail=$mailbox)" homeDirectory | awk '/homeDirectory:/ {print $2}')

        mkdir -p ${target_maildir}Maildir

        ## Rsync data from source_maildir to target_maildir
        rsync -avP -e "ssh -p1797" root@$server:$source_maildir ${target_maildir}Maildir > /dev/null 2>&1

        ## Permission for domain
        chown -R vmail. /var/vmail/vmail1/$domain
        chmod 700 /var/vmail/vmail1/$domain
    done
    echo "- ${green}Done${reset}"
done
echo "----------------------------------------------------------------------------------"

# Return overview's information
echo "[ ${yellow}INFO${reset} ] Return Overview's Information after Rsync"
echo "Domain Total: ${red}$x_total${reset}"
echo "----------------------------------------------------------------------------------"

###########   >_   #####################################################################
res2=$(date +%s.%N)
dt=$(echo "$res2 - $res1" | bc)
dd=$(echo "$dt/86400" | bc)
dt2=$(echo "$dt-86400*$dd" | bc)
dh=$(echo "$dt2/3600" | bc)
dt3=$(echo "$dt2-3600*$dh" | bc)
dm=$(echo "$dt3/60" | bc)
ds=$(echo "$dt3-60*$dm" | bc)
###########   >_   #####################################################################
echo "[ ${green}SUCCESS${reset} ] Rsync Completed"
printf "Total runtime: %d:%02d:%02d:%02.4f\n" $dd $dh $dm $ds
Total_runtime=$(printf "%d:%02d:%02d:%02.4f\n" $dd $dh $dm $ds)
enddate=$(date +%T\ %d-%m-%Y)
echo "End date: $enddate"

###########   >_   #####################################################################
HOSTNAME=$(hostname)
IP=$(ip -4 addr show dev eth0 | sed -ne 's|^.* inet \([^/]*\)/.* scope global.*$|\1|p')
GROUP_ID=-1002041996431
BOT_TOKEN=6343370300:AAFMcxiZQdtQLJSB_dt-0XAPld6jd-r1jcc
TEXT_MESSAGE="
Rsync Data for All Domains from $server to $HOSTNAME - Done
###########   >_   ##################
Tổng thời gian xử lý: $Total_runtime
Thời gian kết thúc: $enddate
"

# Send mess telegram
curl -s --data "text=$TEXT_MESSAGE" --data "chat_id=$GROUP_ID" 'https://api.telegram.org/bot'$BOT_TOKEN'/sendMessage' > /dev/null
}

domain () {
###########   >_   #####################################################################
# Rsync
domain=$1
server=$2
cd /home/ubuntu/iredmail

echo -n "[ ${blue}INFO${reset} ] Running Rsync Data for Domain $domain "

# Define variables
x_path="/home/ubuntu/iredmail/$domain"

x_binddn="cn=Manager,dc=emailserver,dc=vn"

x_basedn="o=domains,dc=emailserver,dc=vn"
    
x_bindpw="4maYJNQGvpMyU4uxIU4HaMvi7K7sPgMH"

# Manage maildir
awk -F ":" '{print $1, $2}' $x_path/export-mailboxes_maildir.$domain | while read mailbox source_maildir
do
    target_maildir=$(ldapsearch -x -o ldif-wrap=no -D $x_binddn -w $x_bindpw -b $x_basedn "(mail=$mailbox)" homeDirectory | awk '/homeDirectory:/ {print $2}')

    mkdir -p ${target_maildir}Maildir

    ## Rsync data from source_maildir to target_maildir
    rsync -avP -e "ssh -p1797" root@$server:$source_maildir ${target_maildir}Maildir > /dev/null 2>&1

    ## Permission for domain
    chown -R vmail. /var/vmail/vmail1/$domain
    chmod 700 /var/vmail/vmail1/$domain
done
echo "- ${green}Done${reset}"
echo "----------------------------------------------------------------------------------"

###########   >_   #####################################################################
res2=$(date +%s.%N)
dt=$(echo "$res2 - $res1" | bc)
dd=$(echo "$dt/86400" | bc)
dt2=$(echo "$dt-86400*$dd" | bc)
dh=$(echo "$dt2/3600" | bc)
dt3=$(echo "$dt2-3600*$dh" | bc)
dm=$(echo "$dt3/60" | bc)
ds=$(echo "$dt3-60*$dm" | bc)
###########   >_   #####################################################################
echo "[ ${green}SUCCESS${reset} ] Rsync Completed"
printf "Total runtime: %d:%02d:%02d:%02.4f\n" $dd $dh $dm $ds
Total_runtime=$(printf "%d:%02d:%02d:%02.4f\n" $dd $dh $dm $ds)
enddate=$(date +%T\ %d-%m-%Y)
echo "End date: $enddate"

###########   >_   #####################################################################
HOSTNAME=$(hostname)
IP=$(ip -4 addr show dev eth0 | sed -ne 's|^.* inet \([^/]*\)/.* scope global.*$|\1|p')
GROUP_ID=-1002041996431
BOT_TOKEN=6343370300:AAFMcxiZQdtQLJSB_dt-0XAPld6jd-r1jcc
TEXT_MESSAGE="
Rsync Data for Domain $domain from $server to $HOSTNAME  - Done
###########   >_   ##################
Tổng thời gian xử lý: $Total_runtime
Thời gian kết thúc: $enddate
"

# Send mess telegram
curl -s --data "text=$TEXT_MESSAGE" --data "chat_id=$GROUP_ID" 'https://api.telegram.org/bot'$BOT_TOKEN'/sendMessage' > /dev/null
}

# Main function, switches options passed to it
case "$1" in
    -h) helptext
    ;;
    --help) helptext

    case "$2" in
        --all|-a) 
        all "$3"
        ;;
        --domain|-d) 
        domain "$3" "$4"
        ;;
    *)
        tput bold
        tput setaf 1
        echo "Invalid Option!"
        helptext
        ;;
    esac
        ;;
        --all|-a)
        if [ -z $2 ]; then
            tput bold
            tput setaf 1
            echo "Invalid Option! Missing arguments for server."
            helptext_all
            exit 1
        elif  [ $# -gt 2 ]; then
            tput bold
            tput setaf 1
            echo "Too many arguments, should only add 1 server argument."
            helptext_all
            exit 1
        else
            all "$2"
        fi
        ;;
        --domain|-d)
        if [ -z $2 ] || [ -z $3 ]; then
            tput bold
            tput setaf 1
            echo "Invalid Option! Missing arguments for domain or server."
            helptext_domain
            exit 1
        elif  [ $# -gt 3 ]; then
            tput bold
            tput setaf 1
            echo "Too many arguments, should only add 1 domain and 1 server argument."
            helptext_domain
            exit 1
        else
        domain "$2" "$3"
        fi
        ;;
    *)
        tput bold
        tput setaf 1
        echo "Invalid Option!"
        helptext
        ;;                
esac