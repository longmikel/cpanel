#!/bin/bash
# Set verbose to null
verbose=""

# Print the help text
helptext () {
    echo "[iRedMail]"
    echo "USAGE: $0 [Options] [Server] or [Options] [Domain] [Server]"
    echo "Options:"
    echo "  -h, --help     display this help and exit"
    echo "  -a, --all      run backup for all domain on server"
    echo "  -d, --domain   select one domain will backup config"
    echo "."
    echo "."
    echo "."
        echo "- Only run 1 time!"
    exit 0
}

helptext_all () {
    echo "[iRedMail]"
    echo "USAGE: $0 [Options] [Domain] [Server]"
    echo "Options:"
    echo "  -h, --help     display this help and exit"
    echo "  -a, --all      run migrate for domain on server old to server new"
    echo "."
    echo "."
    echo "."
        echo "- Only run 1 time!"
    exit 0
}

helptext_sync () {
    echo "[iRedMail]"
    echo "USAGE: $0 [Options] [Domain] [Server]"
    echo "Options:"
    echo "  -h, --help     display this help and exit"
    echo "  -s, --sync     run sync for domain on server old to server new"
    echo "."
    echo "."
    echo "."
        echo "- Only run 1 time!"
    exit 0
}

# Define html
echo "<!DOCTYPE html>
<html lang="vi">
<head>
    <meta charset="UTF-8">
    <title>Hiển thị thông tin màu sắc</title>
    <style>
        /* Định dạng chung */
        body {
            font-family: Arial, sans-serif;
        }

        /* Định dạng cho thông báo */
        .check {
            color: purple; /* Màu tím cho phần kiểm tra */
            font-weight: bold; /* Tăng độ đậm cho phần CHECK */
        }
        .info {
            color: blue; /* Màu xanh dương cho phần thông tin */
            font-weight: bold; /* Tăng độ đậm cho phần INFO */
        }
        .waiting {
            color: orange; /* Màu cam cho phần chờ */
            font-weight: bold; /* Tăng độ đậm cho phần WAITING */
        }
        .error {
            color: red; /* Màu đỏ cho phần lỗi */
            font-weight: bold; /* Tăng độ đậm cho phần ERROR */
        }
    </style>
</head>
</html>
"

all () {
###########   >_   #####################################################################
# Define params
domain=$1
source_server=$2
target_server=$(hostname)
x_tools="/usr/local/src/iRedMail"
x_backup_path_domain="/home/ubuntu/iredmail/$domain"
x_private_key="/root/.ssh/id_migrate"                 
x_binddn="cn=Manager,dc=emailserver,dc=vn"
x_basedn="o=domains,dc=emailserver,dc=vn"
x_bindpw=`cat $x_tools/vault/secret-ldap.txt | openssl enc -aes-256-cbc -md sha512 -a -d -pbkdf2 -iter 100000 \
-salt -pass pass:Secret@123#`

###########   >_   #####################################################################
# Running backup
echo "<!DOCTYPE html>
<html lang="vi">
    <body>
        <p><span class="info">[INFO]</span> - Running Backup domain $domain, please wait...</p>
    </body>
</html>
"
## Running backup config for domain on source_server
sleep 3
if [ ! -f $x_backup_path_domain.'tar.gz' ]; then
    ssh -T -p1797 -i $x_private_key root@$source_server -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no "bash -c '/usr/local/src/cPanel/cPanel-migrate-iRedMail/cPanel-transfer_tool_backup.sh -d $domain $target_server ; exec $SHELL'" > /dev/null 2>&1
## Running copy config for domain from source_server to target_server
    scp -r -i $x_private_key -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -P1797 root@$source_server:/backup/iredmail/$domain.tar.gz /home/ubuntu/iredmail/ > /dev/null 2>&1
fi

###########   >_   #####################################################################
echo "<!DOCTYPE html>
<html lang="vi">
    <body>
        <p><span class="info">[INFO]</span> - Running Restore for Domain $domain, please wait...</p>
    </body>
</html>
"
## Running restore config for domain on target_server
sleep 3
/usr/local/src/iRedMail/cPanel-migrate-iRedMail/iRedMail-transfer_tool_restore.sh -d $domain

###########   >_   #####################################################################
echo "<!DOCTYPE html>
<html lang="vi">
    <body>
        <p><span class="info">[INFO]</span> - Running Rsync Data for Domain $domain from $source_server to $target_server, please wait...</p>
    </body>
</html>
"
## Running rsync data for domain from source_server to target_server
sleep 3
_domain=$(ldapsearch -x -o ldif-wrap=no -D $x_binddn -w $x_bindpw -b $x_basedn domainName | awk '/domainName:/ {print $2}' | awk "/$domain/" | cut -d '.' -f1)
if [ -z "$_domain" ]; then
    exit 1
else
    tmux new-session -d -s $_domain && tmux send-keys -t $_domain "/usr/local/src/iRedMail/cPanel-migrate-iRedMail/iRedMail-transfer_tool_rsync.sh -d $domain $source_server" Enter
fi

###########   >_   #####################################################################
echo "<!DOCTYPE html>
<html lang="vi">
    <body>
        <p><span class="waiting">[WAITING]</span> - Migrate $domain from $source_server to $target_server is running in the background, please wait... for notification when the process is complete</p>
    </body>
</html>
"
}
###########   >_   #####################################################################

# Main function, switches options passed to it
case "$1" in
    -h) helptext
    ;;
    --help) helptext

    case "$2" in
        --all|-a) 
        all "$3" "$4"
        ;;
    *)
        echo "Invalid Option!"
        helptext
        ;;
    esac
        ;;
        --all|-a)
        if [ -z $2 ] || [ -z $3 ]; then
            echo "Invalid Option! Missing arguments for domain or server."
            helptext_all
            exit 1
        elif  [ $# -gt 3 ]; then
            echo "Too many arguments, should only add 1 domain and 1 server argument."
            helptext_all
            exit 1
        else
            all "$2" "$3"
        fi
        ;;
    *)
        echo "Invalid Option!"
        helptext
        ;;                
esac