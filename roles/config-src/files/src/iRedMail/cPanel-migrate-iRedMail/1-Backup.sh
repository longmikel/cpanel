#!/bin/bash

red=`tput setaf 1`
green=`tput setaf 2`
yellow=`tput setaf 3`
blue=`tput setaf 4`
reset=`tput sgr0`
res1=$(date +%s.%N)
me=`basename "$0"`
count=1
###########   >_   #####################################################################

# Set verbose to null
verbose=""

# Print the help text
helptext () {
    tput bold
    tput setaf 2
    echo "[iRedMail]"
    tput bold
    tput setaf 3
    echo "USAGE: $0 [Options] [Server] or [Options] [Domain] [Server]"
    tput bold
    tput setaf 4
    echo "Options:"
    echo "  -h, --help     display this help and exit"
    echo "  -a, --all      run backup for all domain on server"
    echo "  -d, --domain   select one domain will backup config"
    echo "."
    echo "."
    echo "."
        tput bold
        tput setaf 1
        echo "- Only run 1 time!"
    tput sgr0
    exit 0
}

helptext_all () {
    tput bold
    tput setaf 2
    echo "[iRedMail]"
    tput bold
    tput setaf 3
    echo "USAGE: $0 [Options] [Server]"
    tput bold
    tput setaf 4
    echo "Options:"
    echo "  -h, --help     display this help and exit"
    echo "  -a, --all      run backup for all domain on server"
    echo "."
    echo "."
    echo "."
        tput bold
        tput setaf 1
        echo "- Only run 1 time!"
    tput sgr0
    exit 0
}

helptext_domain () {
    tput bold
    tput setaf 2
    echo "[iRedMail]"
    tput bold
    tput setaf 3
    echo "USAGE: $0 [Options] [Domain] [Server]"
    tput bold
    tput setaf 4
    echo "Options:"
    echo "  -h, --help     display this help and exit"
    echo "  -d, --domain   select one domain will backup config"
    echo "."
    echo "."
    echo "."
        tput bold
        tput setaf 1
        echo "- Only run 1 time!"
    tput sgr0
    exit 0
}

all () {
###########   >_   #####################################################################
# Fix file quota if not exits
server=$1
ls /home/*/etc/*/quota | cut -d'/' -f3 | sort -n > /tmp/account_quota_exit
cat /etc/userdomains | awk '{print $2}' | sort -n | sed '/nobody/d' | sed '/: salehost/d' > /tmp/account
diff -u /tmp/account_quota_exit /tmp/account | grep -E '^\+' | sed 's/^+//g' | sed '1d' > /tmp/account_quota_notfound

while IFS= read -r line
do
    echo -n "$line"
    domain=$(cat /etc/userdomains | grep $line | cut -d: -f1)
    touch /home/$line/etc/$domain/quota
    echo -n " ... "
    chown $line.mail /home/$line/etc/$domain/quota
    echo "Done"
done < /tmp/account_quota_notfound

# Delete backup exits for domain
rm -rf /backup/iredmail

# Backup to folder /backup
x_total=$(awk -F ":" '{print $1 $2}' /etc/domainusers | sed '/salehost/d' | wc -l)

# Create backup directory for iredmail
x_backup_path_all="/backup/iredmail"

if [ ! -d $x_backup_path_all ]; then
    mkdir -p $x_backup_path_all
fi

if [ ! -f $x_backup_path_all ]; then
    touch $x_backup_path_all/export-domain_suspend.csv
fi

# Title for csv file when export domain information suspended
echo "TIME,account,DOMAIN,REASON" > $x_backup_path_all/export-domain_suspend.csv

## Check suspend for domain before migrate
awk -F ":" '{print $1 $2}' /etc/domainusers | sed '/salehost/d' | while read account domain
do
    user_suspend=$(whmapi1 --output=jsonpretty listsuspended | jq -r '.data.account[].user' | grep $account)
    if [ -n "$user_suspend" ]
    then
        time_suspend=$(whmapi1 --output=jsonpretty listsuspended | jq -r '.data.account[].time')
        reason_suspend=$(whmapi1 --output=jsonpretty listsuspended | jq -r '.data.account[].reason')
        echo $time_suspend,$user_suspend,$domain,$reason_suspend >> $x_backup_path_all/export-domain_suspend.csv
    fi
done

## Check unsuspend for domain before migrate
whmapi1 --output=jsonpretty listsuspended | jq -r '.data.account[].user' | while read suspendacct
do
    whmapi1 --output=jsonpretty unsuspendacct user=$suspendacct > /dev/null
done

# Report overview server information
HOSTNAME=$(hostname)
IP=$(ip -4 addr show dev eth0 | sed -ne 's|^.* inet \([^/]*\)/.* scope global.*$|\1|p')
CPU=`nproc`
RAM=`free -g | grep "Mem:" | awk '{print$2}'`
DISK_QUOTA=`df -h /home | awk '{print$2}' | sed -n 2p`
DISK_USAGE=`df -h /home | awk '{print$3}' | sed -n 2p`
DISK_FREE=`df -h /home | awk '{print$4}' | sed -n 2p`
DOMAIN=$(find /var/cpanel/users -type f ! -iname "salehost" -print | wc -l)
EMAIL=$(sed "s|:||g" /etc/userdomains | awk '!/^\* nobody/{print $1, $2 | "sort | uniq" }' | awk '{print "cat /home/"$2"/etc/"$1"/passwd"}' | sh 2>/dev/null | wc -l)
echo "Hostname: $HOSTNAME" > $x_backup_path_all/export-"$HOSTNAME"_overview.txt
echo "IP: $IP" >> $x_backup_path_all/export-"$HOSTNAME"_overview.txt
echo "CPU: $CPU Core" >> $x_backup_path_all/export-"$HOSTNAME"_overview.txt
echo "RAM: $RAM GB" >> $x_backup_path_all/export-"$HOSTNAME"_overview.txt
echo "Disk Total: $DISK_QUOTA" >> $x_backup_path_all/export-"$HOSTNAME"_overview.txt
echo "Disk Usage: $DISK_USAGE" >> $x_backup_path_all/export-"$HOSTNAME"_overview.txt
echo "Disk Free: $DISK_FREE" >> $x_backup_path_all/export-"$HOSTNAME"_overview.txt
echo "Domain Total: $DOMAIN" >> $x_backup_path_all/export-"$HOSTNAME"_overview.txt
echo "Email Total: $EMAIL" >> $x_backup_path_all/export-"$HOSTNAME"_overview.txt
echo "----------------------------------------" >> $x_backup_path_all/export-"$HOSTNAME"_overview.txt
echo "DOMAIN,PACKAGE,QUOTA_SIZE,QUOTA_USED,MAILBOX,FORWARDER,FILTER,AUTORESPONDERS,STATUS" >> $x_backup_path_all/export-"$HOSTNAME"_overview.txt
awk -F ": " '{print $1,$2}' /etc/trueuserdomains | grep -v salehost | while read DOMAIN account;
do
    # Return account's package
    PACKAGE=`grep -w $account /etc/userplans|awk -F ": " '{print $2}'`
    # Return account's suspend
    user_suspend=$(awk 'NR!=1' $x_backup_path_all/export-domain_suspend.csv | awk -F ',' '{print $2}' | grep $account)
    [ -n "$user_suspend" ] && STATUS="Disable" || STATUS="Active"
    # Return account's quota size
    QUOTA_SIZE=$(whmapi1 --output=jsonpretty accountsummary user=$account | jq -r '.data.acct[].disklimit')
    # Return account's quota used
    QUOTA_USED=$(whmapi1 --output=jsonpretty accountsummary user=$account | jq -r '.data.acct[].diskused')
    # Return account's mailbox
    MAILBOX=$(uapi --output=jsonpretty --user=$account Email list_pops no_validate=0 skip_main=1 | jq -r '.result.data[].email' | wc -l)
    # Return account's forwarder
    FORWARDER=$(uapi --user=$account Email count_forwarders|awk '$1 ~ /^data/ && !/autorespond/ {print $2}')
    # Return account's filter
    FILTER=$(uapi --user=$account Email count_filters|awk '$1 ~ /^data:/ {print $2}')
    # Return account's autoresponders
    AUTORESPONDERS=$(uapi --output=jsonpretty --user=$account Email count_auto_responders | jq -r '.result.data')
    echo "$DOMAIN,$PACKAGE,$QUOTA_SIZE,$QUOTA_USED,$MAILBOX,$FORWARDER,$FILTER,$AUTORESPONDERS,$STATUS" >> $x_backup_path_all/export-"$HOSTNAME"_overview.txt
done

###########   >_   #####################################################################
# Title for csv file when export domain information
echo domain_name,package_name,quota_size,max_users,max_aliases,max_lists > $x_backup_path_all/export-domain_information.csv                                  

echo "[ ${blue}INFO${reset} ] Running Backup All Domains on Server $HOSTNAME"
awk -F ":" '{print $1 $2}' /etc/domainusers | sed '/salehost/d' | while read account domain
do
    echo -n "Backup - $domain "
    echo -n "- ($count/$x_total) "
    count=$(($count+1))

    # Create backup directory for domain
    if [ ! -d $x_backup_path_domain ]; then
        mkdir -p $x_backup_path_all/$domain
    fi

    x_backup_path_domain="$x_backup_path_all/$domain"

    # Check directory etc exist for domain
    if [ ! -d /home/$account/etc/$domain ] || [ ! -f /home/$account/etc/$domain/shadow ]
    then
        continue
    fi

    # Export all domain information
    domain_name=$(whmapi1 --output=jsonpretty accountsummary user=$account | jq -r '.data.acct[].domain')
    package_name=$(whmapi1 --output=jsonpretty accountsummary user=$account | jq -r '.data.acct[].plan')
    quota_size=$(whmapi1 --output=jsonpretty accountsummary user=$account | jq -r '.data.acct[].disklimit'| sed 's/M//g')
    max_users=$(whmapi1 --output=jsonpretty accountsummary user=$account | jq -r '.data.acct[].maxpop')
    max_aliases=$(whmapi1 --output=jsonpretty accountsummary user=$account | jq -r '.data.acct[].maxlst')
    max_lists=$(whmapi1 --output=jsonpretty accountsummary user=$account | jq -r '.data.acct[].maxlst')

    # Content for csv file
    echo $domain_name,$package_name,$quota_size,$(expr $max_users + 1),$max_aliases,$max_lists >> $x_backup_path_domain/export-domain_information.csv

    # Unsuspend email account incoming, outgoing and login
    uapi --output=jsonpretty --user=$account Email list_pops no_validate=0 skip_main=1 | jq -r '.result.data[] | select(.suspended_login == 1) | .email' | while read mailbox_suspended
    do
        uapi --output=jsonpretty --user=$account Email unsuspend_login email=$mailbox_suspended > /dev/null

        uapi --output=jsonpretty --user=$account Email unsuspend_incoming email=$mailbox_suspended > /dev/null

        uapi --output=jsonpretty --user=$account Email unsuspend_outgoing email=$mailbox_suspended > /dev/null

        echo $mailbox_suspended >> $x_backup_path_domain/export-mailboxes_suspended.$domain.csv
    done

    # Export mailboxes admin with password encode (MD5) of domain
    awk -F ":" '{print $1, $2}' /etc/shadow | awk '$1 ~ /^'$account'/' | while read username_admin password_admin
    do
        # Convert MD5 password hash to CRYPT
        hash={CRYPT}$password_admin

        # Combined mailboxes admin
        echo $domain,$username_admin,$hash,,, >> $x_backup_path_domain/export-mailboxes_information_admin.$domain.csv
    done

    # Export mailboxes with password encode (MD5) of domain
    awk -F ":" '{print $1, $2}' /home/$account/etc/$domain/shadow | while read username password
    do
        # Convert MD5 password hash to CRYPT
        hash={CRYPT}$password

        # Return email account's quota
        quota=$(uapi --output=jsonpretty --user=$account Email get_pop_quota as_bytes=1 email=$username | jq -r '.result.data')

        # Return email account's forward
        forward=$(sed "s|:||g" /etc/valiases/$domain| awk '$1 ~ /^'$username@$domain'/ && !/autorespond/' | awk '{print $2}')

        if [ ! -n "$forward" ]
        then
            # Combined mailboxes with domain, username, hash, quota and not forward
            echo $domain,$username,$hash,,$quota, >> $x_backup_path_domain/export-mailboxes_information_none_forward.$domain.csv
        else
            # Combined mailboxes with domain, username, hash, quota and forward
            echo $domain,$username,$hash,,$quota,,$forward >> $x_backup_path_domain/export-mailboxes_information_forward.$domain.csv
        fi

        # Check directory mail exist for domain
        if [ ! -d /home/$account/mail ] || [ ! -d /home/$account/mail/$domain ]
        then
            continue
        fi

        # Combine mailboxes with maildir
        echo $username@$domain:/home/$account/mail/$domain/$username/ >> $x_backup_path_domain/export-mailboxes_maildir.$domain
    done

    # Export mailing lists
    uapi --output=jsonpretty --user=$account Email list_lists | jq -r '.result.data[].list' | while read x_list
    do
        # Convert @ -> _ for lists
        list=$(echo $x_list | sed 's/@/_/g')

        # Return mailing list's subscribers
        member=$(/usr/local/cpanel/3rdparty/mailman/bin/list_members $list)

        if [ ! -n "$member" ]
        then
            continue
        fi
        # Combine mailing lists with lists and member
        echo $member >> $x_backup_path_domain/export-list_$x_list
    done

    # Check suspend for domain before backup account
    awk 'NR!=1' $x_backup_path_domain/export-domain_suspend.csv | awk -F "," '{print $2, $4}' | while read user_suspend reason_suspend
    do
        whmapi1 --output=jsonpretty suspendacct user=$user_suspend reason="$reason_suspend" > /dev/null
    done

    # Compress all files in directory backup
    cd /backup/iredmail

    tar czf $domain.tar.gz $domain

    rm -rf $domain
    echo "- ${green}Done${reset}"
done
echo "-----------------------------------------------------------------------"

# Return overview's information
echo "[ ${yellow}INFO${reset} ] Return Overview's Information after Backup"
echo "Hostname: ${red}$HOSTNAME${reset}"
echo "IP: ${red}$IP${reset}"
echo "CPU: ${red}$CPU${reset} ${green}Core${reset}"
echo "RAM: ${red}$RAM${reset} ${green}GB${reset}"
echo "Disk Total: ${red}$DISK_QUOTA${reset}"
echo "Disk Usage: ${red}$DISK_USAGE${reset}"
echo "Disk Free: ${red}$DISK_FREE${reset}"
echo "Domain Total: ${red}$DOMAIN${reset}"
echo "Email Total: ${red}$EMAIL${reset}"
echo "-----------------------------------------------------------------------"

###########   >_   #####################################################################
# Scp to new server
echo "[ ${blue}INFO${reset} ] Copy Backup Config for All Domains to $server - ${green}Done${reset}"
scp -r -P1797 -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no /backup/iredmail/ root@$server:/home/ubuntu > /dev/null 2>&1
echo "-----------------------------------------------------------------------"

###########   >_   #####################################################################
res2=$(date +%s.%N)
dt=$(echo "$res2 - $res1" | bc)
dd=$(echo "$dt/86400" | bc)
dt2=$(echo "$dt-86400*$dd" | bc)
dh=$(echo "$dt2/3600" | bc)
dt3=$(echo "$dt2-3600*$dh" | bc)
dm=$(echo "$dt3/60" | bc)
ds=$(echo "$dt3-60*$dm" | bc)
###########   >_   #####################################################################
echo "[ ${green}SUCCESS${reset} ] Backup Domain Completed"
printf "Total runtime: %d:%02d:%02d:%02.4f\n" $dd $dh $dm $ds
Total_runtime=$(printf "%d:%02d:%02d:%02.4f\n" $dd $dh $dm $ds)
enddate=$(date +%T\ %d-%m-%Y)
echo "End date: $enddate"

###########   >_   #####################################################################
GROUP_ID=-1002041996431
BOT_TOKEN=6343370300:AAFMcxiZQdtQLJSB_dt-0XAPld6jd-r1jcc
TEXT_MESSAGE="
Backup Config for All Domains to $server - Done
###########   >_   ##################
Tổng thời gian xử lý: $Total_runtime
Thời gian kết thúc: $enddate
"

# Send mess telegram
curl -s --data "text=$TEXT_MESSAGE" --data "chat_id=$GROUP_ID" 'https://api.telegram.org/bot'$BOT_TOKEN'/sendMessage' > /dev/null
}

domain () {
###########   >_   #####################################################################
# Fix file quota if not exits
domain=$1
server=$2
account="$(cat /etc/userdomains | grep "${domain}" | cut -d' ' -f2)"
ls /home/${account}/etc/${domain}/quota | cut -d'/' -f3 | sort -n > /tmp/account_quota_exit
grep $domain /etc/userdomains | awk '{print $2}' > /tmp/account
diff -u /tmp/account_quota_exit /tmp/account | grep -E '^\+' | sed 's/^+//g' | sed '1d' > /tmp/account_quota_notfound

while IFS= read -r line
do
    echo -n "$line"
    touch /home/$line/etc/$domain/quota
    echo -n " ... "
    chown $line.mail /home/$line/etc/$domain/quota
    echo "Done"
done < /tmp/account_quota_notfound

# Create backup directory
## For iRedMail
x_backup_path_iredmail="/home/${account}/iredmail"

if [ ! -d $x_backup_path_iredmail ]; then 
    mkdir -p $x_backup_path_iredmail
fi

## For Domain
x_backup_path_domain="/home/${account}/iredmail/$domain"

if [ ! -d $x_backup_path_domain ]; then
    mkdir -p $x_backup_path_domain
fi

if [ ! -f $x_backup_path_domain/export-domain_suspend.csv ]; then
    touch $x_backup_path_domain/export-domain_suspend.csv
fi

# Report overview domain information
## Return domain name
DOMAIN=$domain
## Return account's package
PACKAGE=`grep -w $account /etc/userplans|awk -F ": " '{print $2}'`
## Return account's suspend
user_suspend=$(awk 'NR!=1' $x_backup_path_domain/export-domain_suspend.csv | awk -F ',' '{print $2}' | grep $account)
[ -n "$user_suspend" ] && STATUS="Disable" || STATUS="Active"
## Return account's quota size
QUOTA_SIZE=$(whmapi1 --output=jsonpretty accountsummary user=$account | jq -r '.data.acct[].disklimit')
## Return account's quota used
QUOTA_USED=$(whmapi1 --output=jsonpretty accountsummary user=$account | jq -r '.data.acct[].diskused')
## Return account's mailbox
MAILBOX=$(uapi --output=jsonpretty --user=$account Email list_pops no_validate=0 skip_main=1 | jq -r '.result.data[].email' | wc -l)
## Return account's forwarder
FORWARDER=$(uapi --user=$account Email count_forwarders|awk '$1 ~ /^data/ && !/autorespond/ {print $2}')
## Return account's filter
FILTER=$(uapi --user=$account Email count_filters|awk '$1 ~ /^data:/ {print $2}')
## Return account's autoresponders
AUTORESPONDERS=$(uapi --output=jsonpretty --user=$account Email count_auto_responders | jq -r '.result.data')
echo "$DOMAIN,$PACKAGE,$QUOTA_SIZE,$QUOTA_USED,$MAILBOX,$FORWARDER,$FILTER,$AUTORESPONDERS,$STATUS" >> $x_backup_path_domain/export-domain_overview.txt

###########   >_   #####################################################################
# Title for csv file when export domain information
echo domain_name,package_name,quota_size,max_users,max_aliases,max_lists > $x_backup_path_domain/export-domain_information.csv                                  

echo -n "[ ${blue}INFO${reset} ] Running Backup for Domain $domain "

    ## Check directory etc exist for domain
    if [ ! -d /home/$account/etc/$domain ] || [ ! -f /home/$account/etc/$domain/shadow ]
    then
        continue
    fi

    ## Export all domain information
    domain_name=$(whmapi1 --output=jsonpretty accountsummary user=$account | jq -r '.data.acct[].domain')
    package_name=$(whmapi1 --output=jsonpretty accountsummary user=$account | jq -r '.data.acct[].plan')
    quota_size=$(whmapi1 --output=jsonpretty accountsummary user=$account | jq -r '.data.acct[].disklimit'| sed 's/M//g')
    max_users=$(whmapi1 --output=jsonpretty accountsummary user=$account | jq -r '.data.acct[].maxpop')
    max_aliases=$(whmapi1 --output=jsonpretty accountsummary user=$account | jq -r '.data.acct[].maxlst')
    max_lists=$(whmapi1 --output=jsonpretty accountsummary user=$account | jq -r '.data.acct[].maxlst')

    ## Content for csv file
    echo $domain_name,$package_name,$quota_size,$(expr $max_users + 1),$max_aliases,$max_lists >> $x_backup_path_domain/export-domain_information.csv

    ## Unsuspend email account incoming, outgoing and login
    uapi --output=jsonpretty --user=$account Email list_pops no_validate=0 skip_main=1 | jq -r '.result.data[] | select(.suspended_login == 1) | .email' | while read mailbox_suspended
    do
        uapi --output=jsonpretty --user=$account Email unsuspend_login email=$mailbox_suspended > /dev/null

        uapi --output=jsonpretty --user=$account Email unsuspend_incoming email=$mailbox_suspended > /dev/null

        uapi --output=jsonpretty --user=$account Email unsuspend_outgoing email=$mailbox_suspended > /dev/null

        echo $mailbox_suspended >> $x_backup_path_domain/export-mailboxes_suspended.$domain.csv
    done

    ## Export mailboxes admin with password encode (MD5) of domain
    awk -F ":" '{print $1, $2}' /etc/shadow | awk '$1 ~ /^'$account'/' | while read username_admin password_admin
    do
        ### Convert MD5 password hash to CRYPT
        hash={CRYPT}$password_admin

        ### Combined mailboxes admin
        echo $domain,$username_admin,$hash,,, >> $x_backup_path_domain/export-mailboxes_information_admin.$domain.csv
    done

    ## Export mailboxes with password encode (MD5) of domain
    awk -F ":" '{print $1, $2}' /home/$account/etc/$domain/shadow | while read username password
    do
        ### Convert MD5 password hash to CRYPT
        hash={CRYPT}$password

        ### Return email account's quota
        quota=$(uapi --output=jsonpretty --user=$account Email get_pop_quota as_bytes=1 email=$username | jq -r '.result.data')

        ### Return email account's forward
        forward=$(sed "s|:||g" /etc/valiases/$domain| awk '$1 ~ /^'$username@$domain'/ && !/autorespond/' | awk '{print $2}')

        if [ ! -n "$forward" ]
        then
            ### Combined mailboxes with domain, username, hash, quota and not forward
            echo $domain,$username,$hash,,$quota, >> $x_backup_path_domain/export-mailboxes_information_none_forward.$domain.csv
        else
            ### Combined mailboxes with domain, username, hash, quota and forward
            echo $domain,$username,$hash,,$quota,,$forward >> $x_backup_path_domain/export-mailboxes_information_forward.$domain.csv
        fi

        ### Check directory mail exist for domain
        if [ ! -d /home/$account/mail ] || [ ! -d /home/$account/mail/$domain ]
        then
            continue
        fi

        ### Combine mailboxes with maildir
        echo $username@$domain:/home/$account/mail/$domain/$username/ >> $x_backup_path_domain/export-mailboxes_maildir.$domain
    done

    ## Export mailing lists
    uapi --output=jsonpretty --user=$account Email list_lists | jq -r '.result.data[].list' | while read x_list
    do
        ### Convert @ -> _ for lists
        list=$(echo $x_list | sed 's/@/_/g')

        ### Return mailing list's subscribers
        member=$(/usr/local/cpanel/3rdparty/mailman/bin/list_members $list)

        if [ ! -n "$member" ]
        then
            continue
        fi
        ### Combine mailing lists with lists and member
        echo $member >> $x_backup_path_domain/export-list_$x_list
    done

    ## Compress all files in directory backup
    cd $x_backup_path_iredmail

    tar czf $domain.tar.gz $domain

    rm -rf $domain
    echo "- ${green}Done${reset}"
echo "-----------------------------------------------------------------------"

# Return overview's information
echo "[ ${yellow}INFO${reset} ] Return Overview's Information after Backup"
echo "Domain: ${red}$1${reset}"
echo "Package: ${red}$PACKAGE${reset}"
echo "Quota Size: ${red}$QUOTA_SIZE${reset}"
echo "Quota Used: ${red}$QUOTA_USED${reset}"
echo "Mailbox: ${red}$MAILBOX${reset}"
echo "Forwarder: ${red}$FORWARDER${reset}"
echo "Filter: ${red}$FILTER${reset}"
echo "Autoresponders: ${red}$AUTORESPONDERS${reset}"
echo "Status: ${red}$STATUS${reset}"
echo "-----------------------------------------------------------------------"

###########   >_   #####################################################################
# SCP to New Server
echo "[ ${blue}INFO${reset} ] Copy Backup Config for Domain $domain to $server - ${green}Done${reset}"
scp -r -P1797 -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no $x_backup_path_domain.tar.gz root@$server:/home/ubuntu/iredmail/ > /dev/null 2>&1
echo "-----------------------------------------------------------------------"

###########   >_   #####################################################################
res2=$(date +%s.%N)
dt=$(echo "$res2 - $res1" | bc)
dd=$(echo "$dt/86400" | bc)
dt2=$(echo "$dt-86400*$dd" | bc)
dh=$(echo "$dt2/3600" | bc)
dt3=$(echo "$dt2-3600*$dh" | bc)
dm=$(echo "$dt3/60" | bc)
ds=$(echo "$dt3-60*$dm" | bc)
###########   >_   #####################################################################
echo "[ ${green}SUCCESS${reset} ] Backup Domain Completed"
printf "Total runtime: %d:%02d:%02d:%02.4f\n" $dd $dh $dm $ds
Total_runtime=$(printf "%d:%02d:%02d:%02.4f\n" $dd $dh $dm $ds)
enddate=$(date +%T\ %d-%m-%Y)
echo "End date: $enddate"

###########   >_   #####################################################################
GROUP_ID=-1002041996431
BOT_TOKEN=6343370300:AAFMcxiZQdtQLJSB_dt-0XAPld6jd-r1jcc
TEXT_MESSAGE="
Backup Config for Domain $domain to $server - Done
###########   >_   ##################
Tổng thời gian xử lý: $Total_runtime
Thời gian kết thúc: $enddate
"

# Send mess telegram
curl -s --data "text=$TEXT_MESSAGE" --data "chat_id=$GROUP_ID" 'https://api.telegram.org/bot'$BOT_TOKEN'/sendMessage' > /dev/null
}

# Main function, switches options passed to it
case "$1" in
    -h) helptext
    ;;
    --help) helptext

    case "$2" in
        --all|-a) 
        all "$3"
        ;;
        --domain|-d) 
        domain "$3" "$4"
        ;;
    *)
        tput bold
        tput setaf 1
        echo "Invalid Option!"
        helptext
        ;;
    esac
        ;;
        --all|-a)
        if [ -z $2 ]; then
            tput bold
            tput setaf 1
            echo "Invalid Option! Missing arguments for server."
            helptext_all
            exit 1
        elif  [ $# -gt 2 ]; then
            tput bold
            tput setaf 1
            echo "Too many arguments, should only add 1 server argument."
            helptext_all
            exit 1
        else
            all "$2"
        fi
        ;;
        --domain|-d)
        if [ -z $2 ] || [ -z $3 ]; then
            tput bold
            tput setaf 1
            echo "Invalid Option! Missing arguments for domain or server."
            helptext_domain
            exit 1
        elif  [ $# -gt 3 ]; then
            tput bold
            tput setaf 1
            echo "Too many arguments, should only add 1 domain and 1 server argument."
            helptext_domain
            exit 1
        else
        domain "$2" "$3"
        fi
        ;;
    *)
        tput bold
        tput setaf 1
        echo "Invalid Option!"
        helptext
        ;;                
esac