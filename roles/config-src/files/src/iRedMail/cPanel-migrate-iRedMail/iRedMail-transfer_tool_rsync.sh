#!/bin/bash
res1=$(date +%s.%N)
# Set verbose to null
verbose=""

# Print the help text
helptext () {
    echo "[iRedMail]"
    echo "USAGE: $0 [Options] [Domain] [Server]"
    echo "Options:"
    echo "  -h, --help     display this help and exit"
    echo "  -d, --domain   select one domain will rsync data from source server old"
    echo "."
    echo "."
    echo "."
        echo "- Only run 1 time!"
    exit 0
}

domain () {
###########   >_   #####################################################################
# Rsync
domain=$1
server=$2

# Define variables
x_backup_path_iredmail="/home/ubuntu/iredmail"
x_backup_path_domain="$x_backup_path_iredmail/$domain"
x_private_key="/root/.ssh/id_migrate"
x_tools="/usr/local/src/iRedMail"
x_binddn="cn=Manager,dc=emailserver,dc=vn"
x_basedn="o=domains,dc=emailserver,dc=vn"
x_bindpw=`cat $x_tools/vault/secret-ldap.txt | openssl enc -aes-256-cbc -md sha512 -a -d -pbkdf2 -iter 100000 \
-salt -pass pass:Secret@123#`

# Sync data from server old to server new
cd $x_backup_path_iredmail

awk -F ":" '{print $1, $2}' $x_backup_path_domain/export-mailboxes_maildir.$domain | while read mailbox source_maildir
do
    target_maildir=$(ldapsearch -x -o ldif-wrap=no -D $x_binddn -w $x_bindpw -b $x_basedn "(mail=$mailbox)" homeDirectory | awk '/homeDirectory:/ {print $2}')

    mkdir -p ${target_maildir}Maildir

    ## Rsync data from source_maildir to target_maildir
    rsync -avP -e "ssh -p1797 -i $x_private_key -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no" root@$server:$source_maildir ${target_maildir}Maildir > /dev/null 2>&1

    ## Permission for domain
    chown -R vmail. /var/vmail/vmail1/$domain
    chmod 700 /var/vmail/vmail1/$domain
done

# Calculate mailbox quota
awk -F ":" '{print $1}' $x_backup_path_domain/export-mailboxes_maildir.$domain | while read mailbox
do
    doveadm quota recalc -u $mailbox
done

###########   >_   #####################################################################
res2=$(date +%s.%N)
dt=$(echo "$res2 - $res1" | bc)
dd=$(echo "$dt/86400" | bc)
dt2=$(echo "$dt-86400*$dd" | bc)
dh=$(echo "$dt2/3600" | bc)
dt3=$(echo "$dt2-3600*$dh" | bc)
dm=$(echo "$dt3/60" | bc)
ds=$(echo "$dt3-60*$dm" | bc)
###########   >_   #####################################################################
printf "Total runtime: %d:%02d:%02d:%02.4f\n" $dd $dh $dm $ds
Total_runtime=$(printf "%d:%02d:%02d:%02.4f\n" $dd $dh $dm $ds)
enddate=$(date +%T\ %d-%m-%Y)
echo "End date: $enddate"

###########   >_   #####################################################################
HOSTNAME=$(hostname)

# Webhook or Token.
WEBHOOK_URL="https://axysasia.webhook.office.com/webhookb2/4afc8345-87e4-48c8-bfc2-964a2f590585@58bc40e1-9e84-4da4-93a6-de5aa4ef21de/IncomingWebhook/8049f263d28849a89b748e8301cb75a9/346b1119-1344-4b48-9f69-ea9c24dab4a2"
TITLE="Rsync Data for Domain $domain from $server to $HOSTNAME"
TEXT="The data synchronization process is complete within the total processing time: $Total_runtime - $enddate"

# Convert formating.
MESSAGE=$( echo ${TEXT} | sed 's/"/\"/g' | sed "s/'/\'/g" )
JSON="{\"title\": \"${TITLE}\", \"text\": \"${MESSAGE}\" }"

# Post to Microsoft Teams.
curl -H "Content-Type: application/json" -d "${JSON}" "${WEBHOOK_URL}"

# Clean up backup config for domain after sync data
rm -rf /home/ubuntu/iredmail/$domain*

# Out session Tmux after sync data
_domain=$(ldapsearch -x -o ldif-wrap=no -D $x_binddn -w $x_bindpw -b $x_basedn domainName | awk '/domainName:/ {print $2}' | awk "/$domain/" | cut -d '.' -f1)
tmux kill-session -t $_domain
}

# Main function, switches options passed to it
case "$1" in
    -h) helptext
    ;;
    --help) helptext

    case "$2" in
        --domain|-d) 
        domain "$3" "$4"
        ;;
    *)
        echo "Invalid Option!"
        helptext
        ;;
    esac
        ;;
        --domain|-d)
        if [ -z $2 ] || [ -z $3 ]; then
            echo "Invalid Option! Missing arguments for domain or server."
            helptext
            exit 1
        elif  [ $# -gt 3 ]; then
            echo "Too many arguments, should only add 1 domain and 1 server argument."
            helptext
            exit 1
        else
        domain "$2" "$3"
        fi
        ;;
    *)
        echo "Invalid Option!"
        helptext
        ;;                
esac