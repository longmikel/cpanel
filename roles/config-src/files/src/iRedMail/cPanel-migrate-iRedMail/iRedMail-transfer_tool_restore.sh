#!/bin/bash
# Set verbose to null
verbose=""

# Print the help text
helptext () {
    echo "[iRedMail]"
    echo "USAGE: $0 [Options] [Domain]"
    echo "Options:"
    echo "  -h, --help     display this help and exit"
    echo "  -d, --domain   select one domain will restore config"
    echo "."
    echo "."
    echo "."
        echo "- Only run 1 time!"
    exit 0
}

domain () {
###########   >_   #####################################################################
# Restore
domain=$1

# Define variables
x_backup_path_iredmail="/home/ubuntu/iredmail"
x_backup_path_domain="$x_backup_path_iredmail/$domain"
x_tools="/usr/local/src/iRedMail"
x_binddn="cn=Manager,dc=emailserver,dc=vn"
x_basedn="o=domains,dc=emailserver,dc=vn"
x_bindpw=`cat $x_tools/vault/secret-ldap.txt | openssl enc -aes-256-cbc -md sha512 -a -d -pbkdf2 -iter 100000 \
-salt -pass pass:Secret@123#`

# Uncompress file backup for domain
cd $x_backup_path_iredmail

if [ -f $x_backup_path_domain.'tar.gz' ]; then
    tar xvf $domain.tar.gz > /dev/null
fi

x_domain=$(ldapsearch -x -o ldif-wrap=no -D $x_binddn -w $x_bindpw -b $x_basedn domainName | awk '/domainName:/ {print $2}' | awk "/$domain/")
if [ -z "$x_domain" ]
then
    # Create domain
    python3 $x_tools/scripts/iRedMail-create_domain.py $domain > /dev/null
fi

# Manage user for domain
## Import user for domain to become Admin
x_admin=$(find $x_backup_path_domain -iname "export-mailboxes_information_admin*")
if [ -n "$x_admin" ]
then
    # Generates file *.ldif export mailbox without forward
    python3 $x_tools/scripts/iRedMail-create_mailboxes_OpenLDAP_domainadmin.py $x_backup_path_domain/export-mailboxes_information_admin.$domain.csv > /dev/null
    ## Import user from file *.ldif by ldapadd
    x_admin_ldif=$(find $x_backup_path_domain -iname "export-mailboxes_information_admin.$domain.csv.ldif")
    if [ -n "$x_admin_ldif" ]
    then
        # Import user admin
        ldapadd -x -D $x_binddn -w $x_bindpw -f $x_backup_path_domain/export-mailboxes_information_admin.$domain.csv.ldif > /dev/null
        # Become an admin for the domain
        ldapmodify -x -D $x_binddn -w $x_bindpw -f $x_backup_path_domain/export-mailboxes_information_admin.$domain.csv.admin.ldif > /dev/null
    fi
fi

## Import user for domain not have forwarded
x_none_fw=$(find $x_backup_path_domain -iname "export-mailboxes_information_none_forward*")
if [ -n "$x_none_fw" ]
then
    # Generates file *.ldif export mailbox without forward
    python3 $x_tools/scripts/iRedMail-create_mailboxes_OpenLDAP_none_forward.py $x_backup_path_domain/export-mailboxes_information_none_forward.$domain.csv > /dev/null
    ## Import user from file *.ldif by ldapadd
    x_none_fw_ldif=$(find $x_backup_path_domain -iname "export-mailboxes_information_none_forward.$domain.csv.ldif")
    if [ -n "$x_none_fw_ldif" ]
    then
        ldapadd -x -D $x_binddn -w $x_bindpw -f $x_backup_path_domain/export-mailboxes_information_none_forward.$domain.csv.ldif > /dev/null
    fi
fi

## Import user for domain have forwarded
x_fw=$(find $x_backup_path_domain -iname "export-mailboxes_information_forward*")
if [ -n "$x_fw" ]
then
    # Generates file *.ldif export mailbox with forward
    python3 $x_tools/scripts/iRedMail-create_mailboxes_OpenLDAP_forward.py $x_backup_path_domain/export-mailboxes_information_forward.$domain.csv > /dev/null
    # Import user from file *.ldif by ldapadd
    x_fw_ldif=$(find $x_backup_path_domain -iname "export-mailboxes_information_none_forward.$domain.csv.ldif")
    if [ -n "$x_fw_ldif" ]
    then
        ldapadd -x -D $x_binddn -w $x_bindpw -f $x_backup_path_domain/export-mailboxes_information_forward.$domain.csv.ldif > /dev/null
    fi
fi

# Manage mailing lis
x_list=$(find $x_backup_path_domain -iname "export-list_*")
if [ -n "$x_list" ]
then
ls $x_list | sed 's/export-list_//g' | awk -F "/" '{print $6}' | while read list
do
    ## Create mailing list
    python3 /opt/mlmmjadmin/tools/maillist_admin.py create $list disable_archive=no > /dev/null
    cat $x_backup_path_domain/export-list_$list | while read subscriber
    do
        ## Import subscribers to list
        python3 /opt/mlmmjadmin/tools/maillist_admin.py add_subscribers $list $subscriber > /dev/null
    done
done
fi
}

# Main function, switches options passed to it
case "$1" in
    -h) helptext
    ;;
    --help) helptext

    case "$2" in
        --domain|-d)
        domain "$3"
        ;;
    *)
        echo "Invalid Option!"
        helptext
        ;;
    esac
        ;;
        --domain|-d)
        if [ -z $2 ]; then
            echo "Invalid Option! Missing arguments for domain."
            helptext
            exit 1
        elif  [ $# -gt 2 ]; then
            echo "Too many arguments, should only add 1 domain argument."
            helptext
            exit 1
        else
        domain "$2"
        fi
        ;;
    *)
        echo "Invalid Option!"
        helptext
        ;;                
esac