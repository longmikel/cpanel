#!/bin/bash

red=`tput setaf 1`
green=`tput setaf 2`
yellow=`tput setaf 3`
blue=`tput setaf 4`
reset=`tput sgr0`
res1=$(date +%s.%N)
me=`basename "$0"`
count=1
###########   >_   #####################################################################

# Set verbose to null
verbose=""

# Print the help text
helptext () {
    tput bold
    tput setaf 2
    echo "[iRedMail]"
    tput bold
    tput setaf 3
    echo "USAGE: $0 [Options] [Server] or [Options] [Domain] [Server]"
    tput bold
    tput setaf 4
    echo "Options:"
    echo "  -h, --help     display this help and exit"
    echo "  -a, --all      run recalculate mailbox for all domain on server"
    echo "  -d, --domain   select one domain will recalculate mailbox"
    echo "."
    echo "."
    echo "."
        tput bold
        tput setaf 1
        echo "- Only run 1 time!"
    tput sgr0
    exit 0
}

helptext_all () {
    tput bold
    tput setaf 2
    echo "[iRedMail]"
    tput bold
    tput setaf 3
    echo "USAGE: $0 [Options] [Server]"
    tput bold
    tput setaf 4
    echo "Options:"
    echo "  -h, --help     display this help and exit"
    echo "  -a, --all      run recalculate mailbox for all domain on server"
    echo "."
    echo "."
    echo "."
        tput bold
        tput setaf 1
        echo "- Only run 1 time!"
    tput sgr0
    exit 0
}

helptext_domain () {
    tput bold
    tput setaf 2
    echo "[iRedMail]"
    tput bold
    tput setaf 3
    echo "USAGE: $0 [Options] [Domain] [Server]"
    tput bold
    tput setaf 4
    echo "Options:"
    echo "  -h, --help     display this help and exit"
    echo "  -d, --domain   select one domain will recalculate mailbox"
    echo "."
    echo "."
    echo "."
        tput bold
        tput setaf 1
        echo "- Only run 1 time!"
    tput sgr0
    exit 0
}

all () {
###########   >_   #####################################################################
# Dovecot recalculate
cd /home/ubuntu/iredmail
cat /dev/null > list.txt
ls *.tar.gz > list.txt
x_total=$(cat list.txt | wc -l)

echo "[ ${blue}INFO${reset} ] - Recalculate Mailbox Quota for All Domains"
cat list.txt | sed 's/.tar.gz//g' | while read domain
do
    echo -n "Recalculate quota - $domain "
    echo -n "- ($count/$x_total) "
    count=$(($count+1))

    # Define variables
    x_path="/home/ubuntu/iredmail/$domain"
    
    # Manage maildir
    awk -F ":" '{print $1}' $x_path/export-mailboxes_maildir.$domain | while read mailbox
    do
        doveadm quota recalc -u $mailbox
    done
    echo "- ${green}Done${reset}"
done
echo "----------------------------------------------------------------------------------"

# Return overview's information
echo "[ ${yellow}INFO${reset} ] Return Overview's Information after Recalculate"
echo "Domain Total: ${red}$x_total${reset}"
echo "----------------------------------------------------------------------------------"

###########   >_   #####################################################################
res2=$(date +%s.%N)
dt=$(echo "$res2 - $res1" | bc)
dd=$(echo "$dt/86400" | bc)
dt2=$(echo "$dt-86400*$dd" | bc)
dh=$(echo "$dt2/3600" | bc)
dt3=$(echo "$dt2-3600*$dh" | bc)
dm=$(echo "$dt3/60" | bc)
ds=$(echo "$dt3-60*$dm" | bc)
###########   >_   #####################################################################
echo "[ ${green}SUCCESS${reset} ] Recalculate Completed"
printf "Total runtime: %d:%02d:%02d:%02.4f\n" $dd $dh $dm $ds
Total_runtime=$(printf "%d:%02d:%02d:%02.4f\n" $dd $dh $dm $ds)
enddate=$(date +%T\ %d-%m-%Y)
echo "End date: $enddate"

###########   >_   #####################################################################
HOSTNAME=$(hostname)
IP=$(ip -4 addr show dev eth0 | sed -ne 's|^.* inet \([^/]*\)/.* scope global.*$|\1|p')
GROUP_ID=-1002041996431
BOT_TOKEN=6343370300:AAFMcxiZQdtQLJSB_dt-0XAPld6jd-r1jcc
TEXT_MESSAGE="
Recalculate Mailbox Quota for All Domains on $HOSTNAME- Done
###########   >_   ##################
Tổng thời gian xử lý: $Total_runtime
Thời gian kết thúc: $enddate
"

# Send mess telegram
curl -s --data "text=$TEXT_MESSAGE" --data "chat_id=$GROUP_ID" 'https://api.telegram.org/bot'$BOT_TOKEN'/sendMessage' > /dev/null
}

domain () {
###########   >_   #####################################################################
# Dovecot recalculate
domain=$1

echo -n "[ ${blue}INFO${reset} ] - Recalculate Mailbox Quota for Domain $domain "

# Define variables
x_path="/home/ubuntu/iredmail/$domain"

# Manage maildir
awk -F ":" '{print $1}' $x_path/export-mailboxes_maildir.$domain | while read mailbox
do
    doveadm quota recalc -u $mailbox
done
echo "- ${green}Done${reset}"
echo "----------------------------------------------------------------------------------"

###########   >_   #####################################################################
res2=$(date +%s.%N)
dt=$(echo "$res2 - $res1" | bc)
dd=$(echo "$dt/86400" | bc)
dt2=$(echo "$dt-86400*$dd" | bc)
dh=$(echo "$dt2/3600" | bc)
dt3=$(echo "$dt2-3600*$dh" | bc)
dm=$(echo "$dt3/60" | bc)
ds=$(echo "$dt3-60*$dm" | bc)
###########   >_   #####################################################################
echo "[ ${green}SUCCESS${reset} ] Recalculate Completed"
printf "Total runtime: %d:%02d:%02d:%02.4f\n" $dd $dh $dm $ds
Total_runtime=$(printf "%d:%02d:%02d:%02.4f\n" $dd $dh $dm $ds)
enddate=$(date +%T\ %d-%m-%Y)
echo "End date: $enddate"

###########   >_   #####################################################################
HOSTNAME=$(hostname)
IP=$(ip -4 addr show dev eth0 | sed -ne 's|^.* inet \([^/]*\)/.* scope global.*$|\1|p')
GROUP_ID=-1002041996431
BOT_TOKEN=6343370300:AAFMcxiZQdtQLJSB_dt-0XAPld6jd-r1jcc
TEXT_MESSAGE="
Recalculate Mailbox Quota for $domain on $HOSTNAME - Done
###########   >_   ##################
Tổng thời gian xử lý: $Total_runtime
Thời gian kết thúc: $enddate
"

# Send mess telegram
curl -s --data "text=$TEXT_MESSAGE" --data "chat_id=$GROUP_ID" 'https://api.telegram.org/bot'$BOT_TOKEN'/sendMessage' > /dev/null
}

# Main function, switches options passed to it
case "$1" in
    -h) helptext
    ;;
    --help) helptext

    case "$2" in
        --all|-a) 
        all
        ;;
        --domain|-d)
        domain "$3"
        ;;
    *)
        tput bold
        tput setaf 1
        echo "Invalid Option!"
        helptext
        ;;
    esac
        ;;
        --all|-a)
        if [ -z $1 ]; then
            tput bold
            tput setaf 1
            echo "Invalid Option! Missing select options --domain or -d."
            helptext_all
            exit 1
        elif  [ $# -gt 1 ]; then
            tput bold
            tput setaf 1
            echo "Too many arguments, should only add options --domain or -d."
            helptext_all
            exit 1
        else
            all
        fi
        ;;
        --domain|-d)
        if [ -z $2 ]; then
            tput bold
            tput setaf 1
            echo "Invalid Option! Missing arguments for domain."
            helptext_domain
            exit 1
        elif  [ $# -gt 2 ]; then
            tput bold
            tput setaf 1
            echo "Too many arguments, should only add 1 domain argument."
            helptext_domain
            exit 1
        else
        domain "$2"
        fi
        ;;
    *)
        tput bold
        tput setaf 1
        echo "Invalid Option!"
        helptext
        ;;                
esac