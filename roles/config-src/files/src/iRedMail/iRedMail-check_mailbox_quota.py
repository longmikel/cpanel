#!/usr/bin/env python3
import subprocess
import os
import requests
import socket

# Variables for LDAP bind credentials and base DN
x_server_hostname = socket.gethostname()
x_url = f'https://{x_server_hostname}/admin/api'
x_binddn = 'cn=vmail,dc=emailserver,dc=vn'
x_basedn = 'o=domains,dc=emailserver,dc=vn'
x_bindpw = '4ihaDL6QA0AmgDJtxZ1BVexP81LOigYo'
x_admin = 'api@email.emailserver.vn'
x_pw = 'j3ANxPd@ige29trFDIoZYiBXBO'
update_script = '/opt/www/iredadmin/tools/update_mailbox_quota.py'

# Define a class to login and interact with iRedAdmin API
class iRedAdminAPI:
    def __init__(self, base_url, admin_email, admin_password):
        self.base_url = base_url
        self.admin_email = admin_email
        self.admin_password = admin_password
        self.cookies = None

    def login(self):
        login_url = f'{self.base_url}/login'
        response = requests.post(login_url, data={'username': self.admin_email, 'password': self.admin_password})
        data = response.json()
        if data['_success']:
            self.cookies = response.cookies
        return data['_success']

    def get_stored_bytes(self, mailbox):
        user_url = f'{self.base_url}/user/{mailbox}'
        response = requests.get(user_url, cookies=self.cookies)
        if response.status_code == 200:
            data = response.json()
            if '_data' in data and 'stored_bytes' in data['_data']:
                return data['_data']['stored_bytes']
        return None

    def logout(self):
        self.cookies = None

# Function to run a shell command and return output
def run_command(cmd):
    try:
        result = subprocess.run(cmd, shell=True, check=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
        return result.stdout
    except subprocess.CalledProcessError as e:
        print(f"Error running command: {e}")
        return None

# Function to get all domains using ldapsearch
def get_domains():
    cmd = f"ldapsearch -x -o ldif-wrap=no -D {x_binddn} -w {x_bindpw} -b {x_basedn} domainName | awk '/domainName:/ {{print $2}}'"
    output = run_command(cmd)
    if output:
        return output.strip().splitlines()
    return []

# Function to get all mailboxes for a given domain
def get_mailboxes(domain):
    cmd = f'ldapsearch -x -o ldif-wrap=no -D {x_binddn} -w {x_bindpw} -b "ou=Users,domainName={domain},{x_basedn}" mail | awk \'/mail=/\' | cut -d \',\' -f1 | cut -d \' \' -f2 | sed \'s/mail=//g\''
    output = run_command(cmd)
    if output:
        return output.strip().splitlines()
    return []

# Function to get mailquota for a given mailbox
def get_mail_quota(mailbox, domain):
    cmd = f'ldapsearch -x -o ldif-wrap=no -D {x_binddn} -w {x_bindpw} -b "mail={mailbox},ou=Users,domainName={domain},{x_basedn}" mailquota | awk \'/mailQuota/\' | sed \'s/mailQuota: //g\''
    output = run_command(cmd)
    return output.strip() if output else "No quota found"

# Function to write stored_bytes_limit to a domain-specific file
def write_to_file(domain, mailbox, stored_bytes_limit):
    # Output file path based on domain
    output_file = f'/home/ubuntu/iredmail/{domain}.txt'

    # Write mailbox and stored_bytes_limit to the file
    with open(output_file, 'a') as file:
        file.write(f'{mailbox} {stored_bytes_limit}\n')

# Function to run the update_mailbox_quota.py script with domain.txt
def run_update_script(domain):
    domain_file = f'/home/ubuntu/iredmail/{domain}.txt'
    if os.path.exists(domain_file):
        cmd = f'python3 {update_script} {domain_file}'
        try:
            print(f"Running update script for domain: {domain}")
            result = subprocess.run(cmd, shell=True, check=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
            print(result.stdout)
        except subprocess.CalledProcessError as e:
            print(f"Error running update script for {domain}: {e}")
    else:
        print(f"No file found for domain {domain}.")

# Main function to process all domains and mailboxes
def process_domains():
    # Initialize the API and login
    api = iRedAdminAPI(x_url, x_admin, x_pw)

    if not api.login():
        print("Login failed")
        return

    try:
        domains = get_domains()
        for domain in domains:
            print(f"Processing domain: {domain}")
            mailboxes = get_mailboxes(domain)
            for mailbox in mailboxes:
                print(f"Processing mailbox: {mailbox}")

                # Get mail quota
                quota = get_mail_quota(mailbox, domain)

                # Check if quota is 0
                if quota == "0":
                    print(f"Quota is 0 for {mailbox}, checking stored_bytes...")
                    # Get stored_bytes using API
                    stored_bytes = api.get_stored_bytes(mailbox)
                    if stored_bytes is not None:
                        # Calculate stored_bytes_limit by adding 1GB to stored_bytes
                        stored_bytes_limit = stored_bytes + 1073741824  # 1GB = 1073741824 bytes

                        # Write the result to the file
                        write_to_file(domain, mailbox, stored_bytes_limit)
                    else:
                        print(f"Failed to get stored_bytes for {mailbox}")
                else:
                    print(f"Skipping {mailbox}, quota is {quota} (not 0)")

            # Run the update_mailbox_quota.py script for the domain
            run_update_script(domain)

    finally:
        # Logout from the API
        api.logout()

if __name__ == '__main__':
    process_domains()