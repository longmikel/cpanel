#!/bin/bash
# Use domain
if [ -z "$1" ]; then
    echo Usage: $0 {domain}
    exit 1
fi

# Define variables
domain=$1
private_key=$2
certificate=$3
x_tools="/usr/local/src/iRedMail"
x_binddn="cn=Manager,dc=emailserver,dc=vn"
x_basedn="o=domains,dc=emailserver,dc=vn"
x_bindpw=`cat $x_tools/vault/secret-ldap.txt | openssl enc -aes-256-cbc -md sha512 -a -d -pbkdf2 -iter 100000 \
 -salt -pass pass:Secret@123#`
x_certificate_sni_maps="/etc/postfix/sni_maps"
x_certificate_path_domain="/etc/letsencrypt/live"
x_certificate_template_domain="/etc/nginx/templates"
x_certificate_redirect_domain="/etc/nginx/sites-enabled"

# Define html
echo "<!DOCTYPE html>
<html lang="vi">
<head>
    <meta charset="UTF-8">
    <title>Hiển thị thông tin màu sắc</title>
    <style>
        /* Định dạng chung */
        body {
            font-family: Arial, sans-serif;
        }

        /* Định dạng cho thông báo */
        .check {
            color: purple; /* Màu tím cho phần kiểm tra */
            font-weight: bold; /* Tăng độ đậm cho phần CHECK */
        }
        .info {
            color: blue; /* Màu xanh dương cho phần thông tin */
            font-weight: bold; /* Tăng độ đậm cho phần INFO */
        }
        .success {
            color: green; /* Màu xanh lá cho phần hoàn thành */
            font-weight: bold; /* Tăng độ đậm cho phần SUCCESS */
        }
        .error {
            color: red; /* Màu đỏ cho phần lỗi */
            font-weight: bold; /* Tăng độ đậm cho phần ERROR */
        }
    </style>
</head>
</html>
"

# Check domain exist
echo "<!DOCTYPE html>
<html lang="vi">
    <body>
        <p><span class="check">[CHECK]</span> - Preparing to check domain $1, please wait...</p>
    </body>
</html>
"
sleep 3
i=$(ldapsearch -x -o ldif-wrap=no -D $x_binddn -w $x_bindpw -b $x_basedn domainName | awk '/domainName:/ {print $2}' | awk "/$1/")
if [ -z $i ]; then
    echo "<!DOCTYPE html>
    <html lang="vi">
        <body>
            <p><span class="error">[ERROR]</span> - Domain $1 does not exists on the server</p>
        </body>
    </html>
    "
    exit 1
else
    echo "<!DOCTYPE html>
    <html lang="vi">
        <body>
            <p><span class="info">[INFO]</span> - Domain $1 exists on the server</p>
        </body>
    </html>
    "
fi

# Config certificate for domain
## Certificate
sleep 3
if [ ! -d $x_certificate_path_domain/mail.$domain ]; then
    echo "<!DOCTYPE html>
    <html lang="vi">
        <body>
            <p><span class="info">[INFO]</span> - Config a certificate directory for domain $domain</p>
        </body>
    </html>
    "
    ## Create a certificate directory for domain
    mkdir -p $x_certificate_path_domain/mail.$domain

    ## Config certificate for domain
    cp -r $private_key $x_certificate_path_domain/mail.$domain/privkey.pem

    cp -r $certificate $x_certificate_path_domain/mail.$domain/fullchain.pem
else
    ## Config certificate for domain
    cp -r $private_key $x_certificate_path_domain/mail.$domain/privkey.pem

    cp -r $certificate $x_certificate_path_domain/mail.$domain/fullchain.pem
fi

## Certificate for Postfix
sleep 3
echo "<!DOCTYPE html>
<html lang="vi">
    <body>
        <p><span class="info">[INFO]</span> - Config a certificate for domain $domain on postfix</p>
    </body>
</html>
"
sni_maps=$(grep "$domain" "$x_certificate_sni_maps")

if [[ -z $sni_maps ]]; then
    cat << EOF >> $x_certificate_sni_maps
    mail.$domain /etc/letsencrypt/live/mail.$domain/privkey.pem  /etc/letsencrypt/live/mail.$domain/fullchain.pem
EOF
    postmap -F hash:/etc/postfix/sni_maps
fi

## Certificate for Dovecot
sleep 3
echo "<!DOCTYPE html>
<html lang="vi">
    <body>
        <p><span class="info">[INFO]</span> - Config a certificate for domain $domain on dovecot</p>
    </body>
</html>
"

if [ ! -f /etc/dovecot/iredmail/mail.$domain.conf ]; then
    cat << EOF >> /etc/dovecot/iredmail/mail.$domain.conf
    local_name mail.$domain {
    ssl_cert =</etc/letsencrypt/live/mail.$domain/fullchain.pem
    ssl_key =</etc/letsencrypt/live/mail.$domain/privkey.pem
    }
EOF
fi

## Template certificate directory
sleep 3
if [ ! -f $x_certificate_template_domain/mail.$domain/ssl.tmpl ]; then
    echo "<!DOCTYPE html>
    <html lang="vi">
        <body>
            <p><span class="info">[INFO]</span> - Create template a certificate for domain $domain</p>
        </body>
    </html>
    "
    ## Create template a certificate for domain
    mkdir -p $x_certificate_template_domain/mail.$domain/
    touch $x_certificate_template_domain/mail.$domain/ssl.tmpl

    ## Config template a certificate for domain
    cat << EOF >> $x_certificate_template_domain/mail.$domain/ssl.tmpl
ssl_protocols TLSv1.2 TLSv1.3;

# Fix 'The Logjam Attack'.
ssl_ciphers EECDH+CHACHA20:EECDH+AESGCM:EDH+AESGCM:AES256+EECDH;
ssl_prefer_server_ciphers on;
ssl_dhparam /etc/ssl/dh2048_param.pem;

# Greatly improve the performance of keep-alive connections over SSL.
# With this enabled, client is not necessary to do a full SSL-handshake for
# every request, thus saving time and cpu-resources.
ssl_session_cache shared:SSL:10m;

# To use your own ssl cert (e.g. "Let's Encrypt"), please create symbol link to
# ssl cert/key used below, so that we can manage this config file with Ansible.
#
# For example:
#
# rm -f /etc/ssl/private/iRedMail.key
# rm -f /etc/ssl/certs/iRedMail.crt
# ln -s /etc/letsencrypt/live/<domain>/privkey.pem /etc/ssl/private/iRedMail.key
# ln -s /etc/letsencrypt/live/<domain>/fullchain.pem /etc/ssl/certs/iRedMail.crt
#
# To request free "Let's Encrypt" cert, please check our tutorial:
# https://docs.iredmail.org/letsencrypt.html
ssl_certificate /etc/letsencrypt/live/mail.$domain/fullchain.pem;
ssl_certificate_key /etc/letsencrypt/live/mail.$domain/privkey.pem;
EOF
fi

## Redirect certificate for domain
sleep 3
if [ ! -f $x_certificate_redirect_domain/mail.$domain.conf ]; then
    echo "<!DOCTYPE html>
    <html lang="vi">
        <body>
            <p><span class="info">[INFO]</span> - Create template redirect certificate for domain $domain</p>
        </body>
    </html>
    "
    ## Create template redirect certificate for domain
    touch $x_certificate_redirect_domain/mail.$domain.conf

    ## Config template redirect certificate for domain
    cat << EOF >> $x_certificate_redirect_domain/mail.$domain.conf
#
# Note: This file must be loaded before other virtual host config files,
#
# HTTP
server {
    # Listen on ipv4
    listen 80;
    #listen [::]:80;

    server_name mail.$domain;

    # Redirect all insecure http:// requests to https://
    return 301 https://$host$request_uri;
}
#
# Note: This file must be loaded before other virtual host config files,
#
# HTTPS
server {
    listen 443 ssl http2;
    #listen [::]:443 ssl http2;
    server_name mail.$domain;

    root /var/www/html;
    index index.php index.html;

    include /etc/nginx/templates/misc.tmpl;
    include /etc/nginx/templates/mail.$domain/ssl.tmpl;
    include /etc/nginx/templates/iredadmin.tmpl;
    include /etc/nginx/templates/roundcube.tmpl;
    include /etc/nginx/templates/sogo.tmpl;
    include /etc/nginx/templates/netdata.tmpl;
    include /etc/nginx/templates/php-catchall.tmpl;
    include /etc/nginx/templates/stub_status.tmpl;
}
EOF
fi

# Kiểm tra cấu hình Nginx
nginx -t

if [ $? -eq 0 ]; then
    echo "<!DOCTYPE html>
        <html lang="vi">
            <body>
                <p><span class="success">[SUCCESS]</span> - Nginx configuration is valid. Restart nginx, dovecot, postfix services and check again...</p>
            </body>
        </html>
        "
    systemctl restart nginx dovecot postfix
else
    echo "<!DOCTYPE html>
        <html lang="vi">
            <body>
                <p><span class="error">[ERROR]</span> - Nginx configuration is incorrect. Please check again.</p>
            </body>
        </html>
        "
    exit 1
fi