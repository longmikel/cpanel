#!/bin/bash
# Use domain
if [ -z "$1" ] & [ -z "$2" ] ; then
    echo Usage: $0 {domain} {csv}
    exit 1
fi

# Define variables
x_tools="/usr/local/src/iRedMail"
x_binddn="cn=Manager,dc=emailserver,dc=vn"
x_basedn="o=domains,dc=emailserver,dc=vn"
x_bindpw=`cat $x_tools/vault/secret-ldap.txt | openssl enc -aes-256-cbc -md sha512 -a -d -pbkdf2 -iter 100000 \
 -salt -pass pass:Secret@123#`

# Define html
echo "<!DOCTYPE html>
<html lang="vi">
<head>
    <meta charset="UTF-8">
    <title>Hiển thị thông tin màu sắc</title>
    <style>
        /* Định dạng chung */
        body {
            font-family: Arial, sans-serif;
        }

        /* Định dạng cho thông báo */
        .check {
            color: purple; /* Màu tím cho phần kiểm tra */
            font-weight: bold; /* Tăng độ đậm cho phần CHECK */
        }
        .info {
            color: blue; /* Màu xanh dương cho phần thông tin */
            font-weight: bold; /* Tăng độ đậm cho phần INFO */
        }
        .success {
            color: green; /* Màu xanh lá cho phần hoàn thành */
            font-weight: bold; /* Tăng độ đậm cho phần SUCCESS */
        }
        .error {
            color: red; /* Màu đỏ cho phần lỗi */
            font-weight: bold; /* Tăng độ đậm cho phần ERROR */
        }
    </style>
</head>
</html>
"

# Check domain
echo "<!DOCTYPE html>
<html lang="vi">
    <body>
        <p><span class="check">[CHECK]</span> - Preparing to check domain $1, please wait...</p>
    </body>
</html>
"
sleep 3
i=$(ldapsearch -x -o ldif-wrap=no -D $x_binddn -w $x_bindpw -b $x_basedn domainName | awk '/domainName:/ {print $2}' | awk "/$1/")
if [ -z $i ]; then
    echo "<!DOCTYPE html>
    <html lang="vi">
        <body>
            <p><span class="error">[ERROR]</span> - Domain $1 does not exists on the server</p>
        </body>
    </html>
    "
    exit 1
else
    echo "<!DOCTYPE html>
    <html lang="vi">
        <body>
            <p><span class="info">[INFO]</span> - Domain $1 exists on the server</p>
        </body>
    </html>
    "
fi

domain=$1
csv=$2

## Import user for domain not have forwarded
# Generates file *.ldif export mailbox without forward
python3 $x_tools/scripts/iRedMail-create_mailboxes_OpenLDAP_alias.py $csv > /dev/null
## Import user from file *.ldif by ldapadd
if [ -f $csv.ldif ]
then
    echo "<!DOCTYPE html>
    <html lang="vi">
        <body>
            <p><span class="info">[INFO]</span> - Importing aliases according to list from CSV file $2, please wait...</p>
        </body>
    </html>
    "
    ldapadd -x -D $x_binddn -w $x_bindpw -f $csv.ldif > /dev/null
    echo "<!DOCTYPE html>
    <html lang="vi">
        <body>
            <p><span class="success">[SUCCESS]</span> - Import complete</p>
        </body>
    </html>
    "
else
    echo "<!DOCTYPE html>
    <html lang="vi">
        <body>
            <p><span class="error">[ERROR]</span> - The ldif file was not found, please check again...</p>
        </body>
    </html>
    "
    exit 1
fi
exit 0