<?php

/*
This file is included from the main config.inc.php and attempts to ensure
that mandatory values still exist even when the config.inc.php has been modified
*/

$mandatory_plugins = ['cpanellogin','cpanellogout','automatic_addressbook_ng','html5_notifier','archive','calendar','twofactor_gauthenticator','return_to_webmail','carddav','markasjunk'];

foreach( $mandatory_plugins as $plugin ) {
    if(! preg_grep("/$plugin/",$config['plugins']) ) {
        array_push($config['plugins'],$plugin);
    }
}

?>
