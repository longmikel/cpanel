filter {
  # Log received from filebeat input
  grok {
    patterns_dir   => "/etc/logstash/patterns.d"
    match => { "message" => "%{IP:Client_IP} - %{DATA:Identity} \[%{Date:Date}\] .%{WORD:Method} %{URIPATH:URI_Path}.*?=mail&_action=%{WORD:Action}.*? HTTP/%{NUMBER:HTTP_Version}. %{NUMBER:Status_Code} %{NUMBER:Response_Size} .%{REFERRER:Referrer}. \"%{DATA:Agent}\" \"%{DATA}\" %{NUMBER}" }
    tag_on_failure => [ "_grok_audit_audit_access_nomatch" ]
    add_tag        => [ "Audit_Access" ]
  }
  mutate {
    gsub => ["Identity", "%40", "@"]
  }
  
  # Do some data type conversions
  mutate {
    rename => [
      "Client_IP", "Client IP",
      "port", "Port",
      "host", "Instance",
      "HTTP_Version", "HTTP Version",
      "Status_Code", "Code",
      "URI_Path", "Path",
      "Response_Size", "Response Size"
    ]
    convert => [
      "Port", "integer",
      "Code", "integer",
      "Response Size", "integer"
    ]
  }

  # Geolocation Data
  geoip {
    source      => "Client IP"
    ecs_compatibility => disabled
    target      => "geoip"
    add_field   => [ "[location]", "%{[geoip][longitude]}" ]
    add_field   => [ "[location]", "%{[geoip][latitude]}" ]
  }

  mutate {
    rename => [
      "[geoip][country_name]", "Country"
    ]
  }

  # Remove field data
  mutate {
    remove_field => [
      "dsn",
      "ecs",
      "mode",
      "host",
      "message",
      "input",
      "log",
      "path",
      "offset",
      "[geoip][location]",
      "location",
      "[log][file][path]",
      "[event][original]",
      "[geoip][real_region_name]",
      "[geoip][continent_code]",
      "[geoip][area_code]",
      "[geoip][dma_code]",
      "[geoip][ip]",
      "[geoip][country_code2]",
      "[geoip][country_code3]",
      "[geoip][latitude]",
      "[geoip][location.lat]",
      "[geoip][location.lon]",
      "[geoip][longitude]",
      "[geoip][timezone]",
      "[geoip][postal_code]",
      "[geoip][region_code]",
      "[geoip][region_name]",
      "[geoip][city_name]",
      "[geoip][location]"
    ]
    remove_tag => [
      "_grokparsefailure",
      "_grok_audit_audit_access_nomatch",
      "_geoip_lookup_failure"
    ]
  }
}