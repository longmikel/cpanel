## Cron
0 0 * * * /usr/local/src/cPanel-cleanmailman > /dev/null 2>&1
0 3 * * * sh /usr/local/src/cPanel-config-backup > /dev/null 2>&1
15 1 * * * sh /usr/local/src/cPanel-report > /dev/null 2>&1
15 8 * * * sh /usr/local/src/cPanel-report-backup > /dev/null 2>&1

## Crontab
* * * * * root /usr/local/src/cPanel-check-spam > /dev/null 2>&1
0 7 * * * root /usr/local/src/cPanel-growth > /dev/null 2>&1
*/5 * * * * root /usr/local/src/cPanel-linux-checksymlink > /dev/null 2>&1
*/5 * * * * root /usr/local/src/cPanel-linux-check-static-route > /dev/null 2>&1
* 8,13 * * * root /usr/local/src/cPanel-scan-alert-diskusage > /dev/null 2>&1
0 0 * * 1,4 root systemctl restart snmpd.service > /dev/null 2>&1
0 0 * * * root rm -rf /var/cpanel/bandwidth/* > /dev/null 2>&1
